package smoke.belongNG.tests;


import static utilities.MyExtentReports.reports;
import static verify.SoftAssertions.verifyElementIsPresent;
import static verify.SoftAssertions.verifyElementTextIgnoreCase;
import static verify.SoftAssertions.verifyElementTextContains;
import org.openqa.selenium.WebDriver;

import org.testng.annotations.Test;
import com.aventstack.extentreports.ExtentTest;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.belongng.SitecoreLoginPage;
import pages.belongng.BelongAdminLoginPage;
import pages.belongng.HumanresourcesHomepage;
import pages.belongng.KindredLoginPage;
import pages.belongng.SapWelcomePage;

import utilities.InitTests;
import static  driverfactory.Driver.clickElement;
import verify.SoftAssertions;


public class VerifySiteIsUp extends InitTests {

	WebDriver driver=null;
	WebDriver webdriver = null;
	Driver driverFact = new Driver();
	ExtentTest test=null;
	
	public VerifySiteIsUp(String appName) {
		super(appName);
	}
	
	@Test(enabled=true , priority= 1)
	public void verifySiteCoreSite() throws Exception	{		
		new VerifySiteIsUp("Sitecore");			
		try {
			webdriver = driverFact.initWebDriver(BASEURL,BROWSER_TYPE,"local","");
			test = reports.createTest("Verify Sitecore "+System.getProperty("env")+" is up ");
			test.assignCategory("smoke");
			driver=driverFact.getEventDriver(webdriver,test);
			SitecoreLoginPage sitecoreLogin = new SitecoreLoginPage(driver);
			
			
			verifyElementIsPresent(sitecoreLogin.username, test, "Username");
			verifyElementTextIgnoreCase(sitecoreLogin.usernameLabel, "User Name:", test);
			verifyElementIsPresent(sitecoreLogin.password, test, "Password");
			verifyElementTextIgnoreCase(sitecoreLogin.passwordLabel , "Password:", test);
			verifyElementTextIgnoreCase(sitecoreLogin.loginbutton,"Log in",test);
			
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		}finally{
			reports.flush();
			driver.close();
		}
		
	}	
	
	@Test(enabled=true, priority= 2)
	public void verifyBelongAdminSite() throws Exception	 {
		
		new VerifySiteIsUp("BelongAdmin");		
		try {
			webdriver = driverFact.initWebDriver(BASEURL,BROWSER_TYPE,"local","");
			test = reports.createTest("Verify Belong Admin "+System.getProperty("env")+" is Up");
			test.assignCategory("smoke");
			driver=driverFact.getEventDriver(webdriver,test);
			BelongAdminLoginPage belongLogin = 	new BelongAdminLoginPage(driver);
			verifyElementTextIgnoreCase(belongLogin.instructionLabel, "Please enter your details below and click 'Login' to continue.", test);
			verifyElementTextIgnoreCase(belongLogin.welcomeHeader, "Welcome to the Mercer Belong Administration Center", test);
			verifyElementIsPresent(belongLogin.userName, test, "Username");
			verifyElementTextIgnoreCase(belongLogin.userNameText, "Username", test);
			verifyElementIsPresent(belongLogin.password, test, "Password");
			verifyElementTextIgnoreCase(belongLogin.pwdText, "Password", test);
			verifyElementIsPresent(belongLogin.loginBtn, test, "Login Button");
			
		}catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		}finally{
			reports.flush();
			driver.close();
		}		
	}	

	@Test(enabled=true , priority= 3)
	public void verifySapSite() throws Exception {
		new VerifySiteIsUp("SAP");
		try {
			webdriver = driverFact.initWebDriver(BASEURL,BROWSER_TYPE,"local","");
			test = reports.createTest("Verify SAP "+System.getProperty("env")+" is up ");
			test.assignCategory("smoke");
			driver=driverFact.getEventDriver(webdriver,test);
			SapWelcomePage sapWelcome = new SapWelcomePage(driver);
						
			verifyElementTextIgnoreCase(sapWelcome.northAmericaBenifits, "North America Benefits", test);
			verifyElementIsPresent(sapWelcome.rememberMe, test, "Remember me");
			verifyElementIsPresent(sapWelcome.loginBtn, test, "Login Button");
			verifyElementTextIgnoreCase(sapWelcome.loginBtn, "Login", test);
			
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		}finally{
			reports.flush();
			driver.close();
		}
	}

	@Test(enabled=true, priority= 4)
	public void verifyKindredSite() throws Exception	 {
		
		new VerifySiteIsUp("Kindred");		
		try {
			webdriver = driverFact.initWebDriver(BASEURL,BROWSER_TYPE,"local","");
			test = reports.createTest("Verify Kindred "+System.getProperty("env")+" is Up");
			test.assignCategory("smoke");
			driver=driverFact.getEventDriver(webdriver,test);
			KindredLoginPage kindredLogin =	new KindredLoginPage(driver);		
			if(System.getProperty("env").equalsIgnoreCase("Prod")||System.getProperty("env").equalsIgnoreCase("Production")) {		
					
				
				verifyElementIsPresent(kindredLogin.kindredLogo, test, "Kindred");
				verifyElementTextIgnoreCase(kindredLogin.signInHeader, "Sign In", test);
				verifyElementIsPresent(kindredLogin.oktaUserName, test, "Username");			
				verifyElementIsPresent(kindredLogin.oktaPassword, test, "Password");				
				verifyElementIsPresent(kindredLogin.oktaSignin, test, "Signin Button");
				verifyElementTextIgnoreCase(kindredLogin.remmberMe, "Remember me", test);
				verifyElementTextIgnoreCase(kindredLogin.oktaSignin, "Sign In", test);
				
			}else{
				
				verifyElementTextIgnoreCase(kindredLogin.welcomeHeader, "Welcome to Kindred For Me!", test);
				verifyElementTextIgnoreCase(kindredLogin.instructionLabel, "Please enter your details below and click 'Login' to continue.", test);			
				verifyElementIsPresent(kindredLogin.userName, test, "Username");
				verifyElementTextIgnoreCase(kindredLogin.userNameText, "Username", test);
				verifyElementIsPresent(kindredLogin.password, test, "Password");
				verifyElementTextIgnoreCase(kindredLogin.pwdText, "Password", test);
				verifyElementIsPresent(kindredLogin.loginBtn, test, "Login Button");
			}
			
		}catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		}finally{
			reports.flush();
			driver.close();
		}		
	}
	
	@Test(enabled=true, priority= 5)
	public void verifyGettySite() throws Exception	 {
		if(System.getProperty("env").equalsIgnoreCase("uat")) {
			System.out.println("Skipping getty in UAT");
		}else {
			new VerifySiteIsUp("Getty");		
			try {
				webdriver = driverFact.initWebDriver(BASEURL,BROWSER_TYPE,"local","");
				test = reports.createTest("Verify Getty "+System.getProperty("env")+" is Up");
				test.assignCategory("smoke");
				driver=driverFact.getEventDriver(webdriver,test);
				HumanresourcesHomepage gettyHome=new HumanresourcesHomepage(driver);
				//
				verifyElementIsPresent( gettyHome.searchIcon, test, "Search Icon");
				verifyElementIsPresent(gettyHome.MenuIcon, test, "Menu");
				verifyElementTextIgnoreCase(gettyHome.MenuIcon, "Menu", test);
				verifyElementTextContains(gettyHome.notification, "Getty", test);
				verifyElementIsPresent(gettyHome.clientLogoInHeader, test, "Client Logo");
				verifyElementIsPresent(gettyHome.searchIcon, test, "Search Icon");
				clickElement(gettyHome.searchIcon);
				gettyHome.searchWord("Medical");
				verifyElementTextContains(gettyHome.searchResult, "Medical", test);
				
			}catch (Error e) {
				e.printStackTrace();
				SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test);
				ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
				softAssert.assertAll();
			} catch (Exception e) {
				e.printStackTrace();
				SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test);
				ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
				softAssert.assertAll();
			}finally{
				reports.flush();
				driver.close();
			}
		}
				
	}
}
