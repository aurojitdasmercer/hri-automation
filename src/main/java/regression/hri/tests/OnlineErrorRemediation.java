package regression.hri.tests;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.waitForElementToDisappear;
import static driverfactory.Driver.waitForElementToDisplay;
import static utilities.DatabaseUtils.getResultSet;
import static utilities.MyExtentReports.reports;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;
import com.aventstack.extentreports.ExtentTest;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.hri.ConsultantPortal;
import pages.hri.EmployeeDetailsPage;
import pages.hri.HrAdminPage;
import pages.hri.LoginPage;
import utilities.DatabaseUtils;
import utilities.InitTests;
import verify.SoftAssertions;

import static verify.SoftAssertions.assertNotNull;
import static verify.SoftAssertions.verifyEquals;
import static  verify.SoftAssertions.verifyEqualsIgnoreCase;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class OnlineErrorRemediation extends InitTests {

	WebDriver driver=null;
	WebDriver webdriver = null;
	Driver driverFact = new Driver();
	ExtentTest test=null;
	String companyId;
	
	public OnlineErrorRemediation(String appName) {
		super(appName);	
	}
	
	
	@Test(enabled=true , priority= 10)
	public void verifyOnlineErrorRemediation() throws Exception	{		
		new OnlineErrorRemediation("HRI");			
		try {
			webdriver = driverFact.initWebDriver(BASEURL,BROWSER_TYPE,"local","");
			test = reports.createTest("verifyOnlineErrorRemediation");
			test.assignCategory("Regression");
			driver=driverFact.getEventDriver(webdriver,test);
			
			LoginPage login =  new LoginPage(driver);			
			login.login(USERNAME,PASSWORD);	
			
			
			String cmnyID = "42214";			
			ConsultantPortal cp = new ConsultantPortal(driver);
			FileOperations file  =new FileOperations();
			HrAdminPage hr = new HrAdminPage(driver);			
			cp.navigateToAccesstestingPortal(driver,cmnyID);			
			
			Set<String> handles =driver.getWindowHandles();			
			handles.remove(driver.getWindowHandle());			
			String handle = handles.iterator().next();			
			driver.switchTo().window(handle);
			System.out.println(driver.getCurrentUrl());
			waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));	
			
			
			hr.navigateToFileUpload(driver);
			/*hr.exportPopulation(driver, file, cmnyID);
			file.updateFile(cmnyID);*/
			
			String userdir = System.getProperty("user.dir");
			String filePath = userdir+"\\src\\main\\resources\\testdata\\EmployeeData_Error.xlsx";
			hr.fileUpload(driver,filePath);	
			hr.fixError(driver,"online",file);
			
			verifyEquals(hr.uploadNumber.getText(), "3", test);
			verifyEquals(hr.validationSuccess.getText(), "Upload & Validation Success!", test);
			
			
			
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		}finally{
			reports.flush();
			driver.quit();
		}
		
	}	
}
