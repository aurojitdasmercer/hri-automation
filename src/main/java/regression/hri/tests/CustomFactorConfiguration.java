package regression.hri.tests;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.waitForElementToDisappear;
import static driverfactory.Driver.waitForElementToDisplay;
import static utilities.DatabaseUtils.getResultSet;
import static utilities.MyExtentReports.reports;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;
import com.aventstack.extentreports.ExtentTest;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.hri.ConsultantPortal;
import pages.hri.LoginPage;
import utilities.DatabaseUtils;
import utilities.InitTests;
import verify.SoftAssertions;

import static verify.SoftAssertions.assertNotNull;
import static verify.SoftAssertions.verifyEquals;
import static  verify.SoftAssertions.verifyEqualsIgnoreCase;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.List;

public class CustomFactorConfiguration extends InitTests {

	WebDriver driver=null;
	WebDriver webdriver = null;
	Driver driverFact = new Driver();
	ExtentTest test=null;
	String companyId;
	
	public CustomFactorConfiguration(String appName) {
		super(appName);	
	}
	
	
	@Test(enabled=true , priority= 3)
	public void verifyCustomFactor() throws Exception	{		
		new CustomFactorConfiguration("HRI");			
		try {
			webdriver = driverFact.initWebDriver(BASEURL,BROWSER_TYPE,"local","");
			test = reports.createTest("Verify Custom Factor Configuration");
			test.assignCategory("Regression");
			driver=driverFact.getEventDriver(webdriver,test);
			FileOperations file = new FileOperations();
			
			
			LoginPage login =  new LoginPage(driver);
			login.login(USERNAME,PASSWORD);
			
			ConsultantPortal cp = new ConsultantPortal(driver);			
			cp.addNewClient(driver);			
			cp.createClientProfile(driver,"Enhanced","Y");
			cp.selectPlanDescisions(driver);
			cp.selectCustomFactors(driver);			
			cp.selectBenifits(driver,file);
			
			String actual;
			String expected;
			for(int i =0;i<file.maxCell-1;i++ ) {
				actual = file.sheet_obj.getRow(0).getCell(i).getStringCellValue();
				expected = file.defaultSheet.getRow(0).getCell(i).getStringCellValue();
				verifyEqualsIgnoreCase(actual, expected, test);
				if(actual.equalsIgnoreCase(expected)) {
					System.out.println(actual +"= "+ expected);
				}				
			}
			
			file.updateFile();
			cp.electionMappingUpload.sendKeys(file.filePath);
			clickElement(cp.electionMappingUploadButton);
			clickElement(cp.electionMappingContinue);
			waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));
			
			cp.selectContributionStrategy(driver);
			cp.selectRates(driver);		
			cp.setAdministration(driver);
			
			companyId=  cp.cmpnyId;
			
					
			clickElement(cp.demographicUploadNext2);				
			waitForElementToDisplay(By.cssSelector("div[class='processing-request']"));
			waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));
			clickElement(cp.automatedLoadSaveandContinue);
			waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));
			
			cp.setAcountStructure(driver);
			
			
			
			
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		}finally{
			reports.flush();
			driver.close();
		}
		
	}

}
