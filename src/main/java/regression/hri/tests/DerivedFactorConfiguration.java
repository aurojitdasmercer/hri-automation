package regression.hri.tests;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.waitForElementToDisappear;
import static driverfactory.Driver.waitForElementToDisplay;
import static utilities.DatabaseUtils.getResultSet;
import static utilities.MyExtentReports.reports;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;
import com.aventstack.extentreports.ExtentTest;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.hri.ConsultantPortal;
import pages.hri.LoginPage;
import utilities.DatabaseUtils;
import utilities.InitTests;
import verify.SoftAssertions;

import static verify.SoftAssertions.assertNotNull;
import static verify.SoftAssertions.verifyEquals;
import static  verify.SoftAssertions.verifyEqualsIgnoreCase;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.List;

public class DerivedFactorConfiguration extends InitTests {

	WebDriver driver=null;
	WebDriver webdriver = null;
	Driver driverFact = new Driver();
	ExtentTest test=null;
	String companyId;
	
	public DerivedFactorConfiguration(String appName) {
		super(appName);	
	}
	
	
	@Test(enabled=true , priority= 4)
	public void verifyDerivedFactor() throws Exception	{		
		new DerivedFactorConfiguration("HRI");			
		try {
			webdriver = driverFact.initWebDriver(BASEURL,BROWSER_TYPE,"local","");
			test = reports.createTest("Verify Derived Factor Configuration");
			test.assignCategory("Regression");
			driver=driverFact.getEventDriver(webdriver,test);
			FileOperations file = new FileOperations();
			
			
			LoginPage login =  new LoginPage(driver);
			login.login(USERNAME,PASSWORD);
			
			ConsultantPortal cp = new ConsultantPortal(driver);			
			cp.addNewClient(driver);			
			cp.createClientProfile(driver,"Enhanced","Y");
			cp.selectPlanDescisions(driver);
			
			cp.selectDerivedFactors(driver);
			cp.selectBenifits(driver,file);
			
			String actual;
			String expected;
			for(int i =0;i<file.maxCell-1;i++ ) {
				actual = file.sheet_obj.getRow(0).getCell(i).getStringCellValue();
				expected = file.defaultSheet.getRow(0).getCell(i).getStringCellValue();
				verifyEqualsIgnoreCase(actual, expected, test);
				if(actual.equalsIgnoreCase(expected)) {
					System.out.println(actual +"= "+ expected);
				}				
			}
			
			file.updateFile();
			cp.electionMappingUpload.sendKeys(file.filePath);
			clickElement(cp.electionMappingUploadButton);
			clickElement(cp.electionMappingContinue);
			waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));
			
			cp.selectContributionStrategy(driver);
			cp.selectRates(driver);		
			cp.setAdministration(driver);
			
			companyId=  cp.cmpnyId;
			
					
			clickElement(cp.demographicUploadNext2);				
			waitForElementToDisplay(By.cssSelector("div[class='processing-request']"));
			waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));
			clickElement(cp.automatedLoadSaveandContinue);
			waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));
			
			cp.setAcountStructure(driver);
			
			//companyId = "41500";	//27
			
			Connection mycon= DatabaseUtils.getConnection("oracle.jdbc.driver.OracleDriver", 
					  "jdbc:oracle:thin:@ldap://oid.mercerhrs.com:389/bwqa14,cn=OracleContext,dc=world","Z_MM_MT1_A","MM_MT1_A_zqaFri2019");
			 
			  		System.out.println("Running query1...");
				  ResultSet query1= getResultSet(mycon, "select * from COMPANY_EMPLOYMENT_DATA t where t.ssn in (SELECT SSN FROM COMP_SSN WHERE COMPANY_ID ='"+companyId+"')");
				  
				  List<String> derivedFact1 = new ArrayList<>();	//Latest Hire Date				  
				  List<String> derivedFact2 = new ArrayList<>();	//Current Salary
				  List<String> derivedFact3 = new ArrayList<>();	//Employement Status
				  List<String> derivedFact4 = new ArrayList<>();	//First Name
				  List<String> derivedFact5 = new ArrayList<>();	//Gender
				  
				  while(query1.next()) {
					  System.out.println(query1.getString(27)+" "+(query1.getString(28))+" "+query1.getString(29)+" "+(query1.getString(30))+" "+query1.getString(31));
					  derivedFact1.add(query1.getString(27));
					  derivedFact2.add(query1.getString(28));
					  derivedFact3.add(query1.getString(29));
					  derivedFact4.add(query1.getString(30));
					  derivedFact5.add(query1.getString(31));					  
				  }
				  
				  List<String> latestHireDate = new ArrayList<>();				  
				  List<String> currentSalary = new ArrayList<>();	
				  List<String> empStatus = new ArrayList<>();	
				  List<String> fname = new ArrayList<>();	
				  List<String> gender = new ArrayList<>();
			
				  file.readEmployeeFile();
				  for(int i = 8;i<=file.maxRowEmp;i++){
					  latestHireDate.add(file.sheet_obj_EmpFile.getRow(i).getCell(22).getStringCellValue());
					  currentSalary.add(file.sheet_obj_EmpFile.getRow(i).getCell(34).getStringCellValue());
					  empStatus.add(file.sheet_obj_EmpFile.getRow(i).getCell(26).getStringCellValue());
					  fname.add(file.sheet_obj_EmpFile.getRow(i).getCell(3).getStringCellValue());
					  gender.add(file.sheet_obj_EmpFile.getRow(i).getCell(6).getStringCellValue());					  
					}
				  
				  //---Verifications
				  
				  for(int i=0;i<latestHireDate.size();i++) {
					  
					  verifyEquals(derivedFact1.get(i),latestHireDate.get(i) , test);	
					  
				  }
				  

				  for(int i=0;i<currentSalary.size();i++) {
					  
					  verifyEquals(derivedFact2.get(i),currentSalary.get(i) , test);	
					  
				  }
				  

				  for(int i=0;i<empStatus.size();i++) {
					  
					  verifyEquals(derivedFact3.get(i),empStatus.get(i) , test);	
					  
				  }
				  

				  for(int i=0;i<fname.size();i++) {
					  
					  verifyEquals(derivedFact4.get(i),fname.get(i) , test);	
					  
				  }
				  

				  for(int i=0;i<gender.size();i++) {
					  
					  verifyEquals(derivedFact5.get(i),gender.get(i) , test);	
					  System.out.println(gender.get(i));
					  
				  }
			
			
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		}finally{
			reports.flush();
			driver.close();
		}
		
	}

}
