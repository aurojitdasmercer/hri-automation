package regression.hri.tests;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

import org.apache.poi.sl.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import utilities.ExcelReader;

public class FileOperations {
	ArrayList<String>testcaseList= new ArrayList<String>();
	XSSFWorkbook Workbook_obj = null;
	XSSFWorkbook Workbook_obj2 = null;
	public XSSFSheet sheet_obj=null;
	public XSSFSheet sheet_obj_EmpFile=null;
	public XSSFSheet sheet_obj_StandardEmpFile=null;
	public XSSFSheet defaultSheet=null;
	public int maxCell;
	String sheetName ="Election Mapping";
	String userHome;
	public String filePath;
	String employeeFilePath;
	String userdir;
	public int maxRowEmp;

	public void deleteFile() {
		
		try {
			
			userHome = System.getProperty("user.home");
			filePath = userHome+"\\Downloads\\ElectionMappingTemplate.xlsx";		
			Files.deleteIfExists(Paths.get(filePath)); 
			System.out.println("FileDeleted");			
			
			
		} catch (Exception e) {
			System.out.println("Delete Failed");
		}
		
	}
	
public void deleteFile(String cmpnyID) {
		
		try {
			
			userHome = System.getProperty("user.home");
			filePath = userHome+"\\Downloads\\"+cmpnyID+"_EmployeeData.xlsx";
			System.out.println(filePath);
			Files.deleteIfExists(Paths.get(filePath)); 
			System.out.println("FileDeleted: "+filePath);	
			
		} catch (Exception e) {
			System.out.println("Delete Failed");
		}
		
	}
	
	public void readFile() {
			try {
			
			userHome = System.getProperty("user.home");
			userdir = System.getProperty("user.dir");
			filePath = userHome+"\\Downloads\\ElectionMappingTemplate.xlsx";
			employeeFilePath = userdir+"\\src\\main\\resources\\testdata\\EmployeeData.xlsx";
			System.out.println(filePath);					
			FileInputStream file = new FileInputStream(new File(filePath));	
			FileInputStream file2 = new FileInputStream(new File(userdir+"\\src\\main\\resources\\testdata\\DefaultTemplate.xlsx"));	
		
			System.out.println("Excel file read successfully...");
			Workbook_obj = new XSSFWorkbook(file);
			System.out.println("Before reading Excel sheet...."+sheetName);
			sheet_obj = Workbook_obj.getSheet(sheetName);
			Workbook_obj.close();
			System.out.println("Excel sheet read successfully..."+sheet_obj);
			
			
			System.out.println("Excel file read successfully...");
			Workbook_obj = new XSSFWorkbook(file2);
			System.out.println("Before reading Excel sheet...."+sheetName);
			defaultSheet = Workbook_obj.getSheet(sheetName);
			Workbook_obj.close();
			System.out.println("Excel sheet read successfully..."+sheet_obj);			
			Row r = sheet_obj.getRow(0);
			maxCell=  r.getLastCellNum();
			
		System.out.print("Row count: "+maxCell+"\n");
		for(int i = 0;i<maxCell-1;i++){
			System.out.println(sheet_obj.getRow(0).getCell(i).getStringCellValue());
			System.out.println(defaultSheet.getRow(0).getCell(i).getStringCellValue());
			}			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void updateFile() {
		try {
		XSSFWorkbook Workbook_write = null;
		userHome = System.getProperty("user.home");
		userdir = System.getProperty("user.dir");
		filePath = userHome+"\\Downloads\\ElectionMappingTemplate.xlsx";
		System.out.println(filePath);		
		FileInputStream file = new FileInputStream(new File(filePath));	
		System.out.println("Excel file read successfully...");
		Workbook_write = new XSSFWorkbook(file);
		System.out.println("Before reading Excel sheet...."+sheetName);
		sheet_obj = Workbook_write.getSheet(sheetName);
		XSSFSheet sheet = Workbook_write.getSheetAt(0);
		int rowCount = sheet.getLastRowNum();
		for(int i=1;i<=rowCount;i++) {
			Cell cell2Update = sheet.getRow(i).createCell(9);
			cell2Update.setCellValue("Life");	
		}
		int prodid = 51;
		for(int i=1;i<=rowCount;i++) {
			Cell cell2Update = sheet.getRow(i).createCell(10);
			cell2Update.setCellValue("01-"+prodid);	
			prodid++;
		}		
		file.close();
		FileOutputStream fout = new FileOutputStream(filePath);
		Workbook_write.write(fout);
		Workbook_write.close();
		fout.close();
		System.out.println("FileUpdated");
		
		
		
	} catch (Exception e) {
		e.printStackTrace();
	}
}
	
	public void updateFile(String cmpnyID) {
		try {
		XSSFWorkbook Workbook_write = null;
		userHome = System.getProperty("user.home");
		filePath = userHome+"\\Downloads\\"+cmpnyID+"_EmployeeData.xlsx";		
		userdir = System.getProperty("user.dir");		
		System.out.println(filePath);			
		FileInputStream file = new FileInputStream(new File(filePath));	
		System.out.println("Excel file read successfully...");
		Workbook_write = new XSSFWorkbook(file);
		System.out.println("Before reading Excel sheet...."+"Client File Layout");
		sheet_obj = Workbook_write.getSheet("Client File Layout");
		XSSFSheet sheet = Workbook_write.getSheetAt(4);
		int rowCount = sheet.getLastRowNum();
		System.out.println(rowCount);
		for(int i=8;i<=rowCount;i++) {
			Cell cell2Update = sheet.getRow(i).createCell(0);
			cell2Update.setCellValue("09/15/2018");	
		}
		
		for(int i=8;i<=rowCount;i++) {
			Cell cell2Update = sheet.getRow(i).createCell(34);
			cell2Update.setCellValue("65019");	
			
		}
		
		for(int i=8;i<=rowCount;i++) {
			Cell cell2Update = sheet.getRow(i).createCell(35);
			cell2Update.setCellValue("95519");	
			
		}
		
		
		
		file.close();
		FileOutputStream fout = new FileOutputStream(filePath);
		Workbook_write.write(fout);
		Workbook_write.close();
		fout.close();
		System.out.println("FileUpdated");
		
		
		
	} catch (Exception e) {
		e.printStackTrace();
	}
}
	
	public void readEmployeeFile() throws IOException {
		String userdir = System.getProperty("user.dir");
		String employeeFilePath = userdir+"\\src\\main\\resources\\testdata\\EmployeeData.xlsx";
		FileInputStream file = new FileInputStream(new File(employeeFilePath));	
		System.out.println("Excel file read successfully...");
		Workbook_obj = new XSSFWorkbook(file);
		System.out.println("Before reading Excel sheet...."+sheetName);
		sheet_obj_EmpFile = Workbook_obj.getSheet("Client File Layout");
		Workbook_obj.close();
		System.out.println("Excel sheet read successfully..."+sheet_obj);
		maxRowEmp= sheet_obj_EmpFile.getLastRowNum();	
		System.out.print("Row count: "+maxRowEmp+"\n");
		for(int i = 8;i<=maxRowEmp;i++){
			System.out.println(sheet_obj_EmpFile.getRow(i).getCell(3).getStringCellValue());
		}
	}
	
	public void readStandardEmployeeFile() throws IOException {
		String userdir = System.getProperty("user.dir");
		String employeeFilePath = userdir+"\\src\\main\\resources\\testdata\\EmployeeDataS.xlsx";
		FileInputStream file = new FileInputStream(new File(employeeFilePath));	
		System.out.println("Excel file read successfully...");
		Workbook_obj = new XSSFWorkbook(file);
		System.out.println("Before reading Excel sheet...."+sheetName);
		sheet_obj_StandardEmpFile = Workbook_obj.getSheet("Client File Layout");
		Workbook_obj.close();
		System.out.println("Excel sheet read successfully..."+sheet_obj);
		maxRowEmp= sheet_obj_StandardEmpFile.getLastRowNum();	
		System.out.print("Row count: "+maxRowEmp+"\n");
		DataFormatter format = new DataFormatter();
		for(int i = 8;i<=maxRowEmp;i++){
			
			System.out.println(format.formatCellValue(sheet_obj_StandardEmpFile.getRow(i).getCell(24)));
			//System.out.println(sheet_obj_StandardEmpFile.getRow(i).getCell(24).getStringCellValue());
		}		
		
	}
	
	public void fixErrorFile() {
		try {
			XSSFWorkbook Workbook_write = null;
			userHome = System.getProperty("user.home");
			filePath = userHome+"\\Downloads\\EmployeeData.xlsx";		
			userdir = System.getProperty("user.dir");		
			System.out.println(filePath);			
			FileInputStream file = new FileInputStream(new File(filePath));	
			System.out.println("Excel file read successfully...");
			Workbook_write = new XSSFWorkbook(file);
			System.out.println("Before reading Excel sheet....");
			sheet_obj = Workbook_write.getSheet("Employee Data");
			XSSFSheet sheet = Workbook_write.getSheetAt(0);
			int rowCount = sheet.getLastRowNum();
			System.out.println(rowCount+sheet.getSheetName());
			
			Cell cell2Update;
			
			cell2Update = sheet.getRow(8).createCell(18);
			cell2Update.setCellValue("1234567890");	
		
			//int prodid = 51;
			
			cell2Update = sheet.getRow(9).createCell(19);
			cell2Update.setCellValue("test@mercer.com");
			
			
			cell2Update = sheet.getRow(10).createCell(14);
			cell2Update.setCellValue("USA");	
			
			file.close();
			FileOutputStream fout = new FileOutputStream(filePath);
			Workbook_write.write(fout);
			Workbook_write.close();
			fout.close();
			System.out.println("FileUpdated");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/*public static void main(String args[]) throws IOException {
		
		FileOperations f = new FileOperations();
		//f.deleteFile();
		//f.readFile();
		//f.readEmployeeFile();
		//f.updateFile("40821");
		f.readEmployeeFile();

	}*/

}
