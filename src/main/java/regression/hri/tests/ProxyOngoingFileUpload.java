package regression.hri.tests;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.waitForElementToDisappear;
import static driverfactory.Driver.waitForElementToDisplay;
import static utilities.DatabaseUtils.getResultSet;
import static utilities.MyExtentReports.reports;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;
import com.aventstack.extentreports.ExtentTest;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.hri.ConsultantPortal;
import pages.hri.EmployeeDetailsPage;
import pages.hri.HrAdminPage;
import pages.hri.LoginPage;
import utilities.DatabaseUtils;
import utilities.InitTests;
import verify.SoftAssertions;

import static verify.SoftAssertions.assertNotNull;
import static verify.SoftAssertions.verifyEquals;
import static  verify.SoftAssertions.verifyEqualsIgnoreCase;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class ProxyOngoingFileUpload extends InitTests {

	WebDriver driver=null;
	WebDriver webdriver = null;
	Driver driverFact = new Driver();
	ExtentTest test=null;
	String companyId;
	
	public ProxyOngoingFileUpload(String appName) {
		super(appName);	
	}
	
	
	@Test(enabled=true , priority= 13)
	public void verifyProxyOngoingFileUpload() throws Exception	{		
		new ProxyOngoingFileUpload("HRI");			
		try {
			webdriver = driverFact.initWebDriver(BASEURL,BROWSER_TYPE,"local","");
			test = reports.createTest("Verify Ongoing File Upload throughAccess Testing Portal");
			test.assignCategory("Regression");
			driver=driverFact.getEventDriver(webdriver,test);
			
			LoginPage login =  new LoginPage(driver);			
			login.login(USERNAME,PASSWORD);	
			
			
			String cmnyID = "40821";			
			ConsultantPortal cp = new ConsultantPortal(driver);
			FileOperations file  =new FileOperations();
			HrAdminPage hr = new HrAdminPage(driver);
			
			//--CreateNew CLient
			
			/*cp.addNewClient(driver);			
			cp.createClientProfile(driver,"Enhanced","Y");
			cp.selectPlanDescisions(driver);			
			cp.selectBenifits(driver,file);			
			String actual;
			String expected;
			for(int i =0;i<file.maxCell-1;i++ ) {
				actual = file.sheet_obj.getRow(0).getCell(i).getStringCellValue();
				expected = file.defaultSheet.getRow(0).getCell(i).getStringCellValue();
				verifyEqualsIgnoreCase(actual, expected, test);
				if(actual.equalsIgnoreCase(expected)) {
					System.out.println(actual +"= "+ expected);
				}				
			}
			file.updateFile();
			cp.electionMappingUpload.sendKeys(file.filePath);
			clickElement(cp.electionMappingUploadButton);
			clickElement(cp.electionMappingContinue);
			waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));			
			cp.selectContributionStrategy(driver);
			cp.selectRates(driver);		
			cp.setAdministration(driver);			
			cmnyID=  cp.cmpnyId;			
			clickElement(cp.demographicUploadNext2);				
			waitForElementToDisplay(By.cssSelector("div[class='processing-request']"));
			waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));
			clickElement(cp.automatedLoadSaveandContinue);
			waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));			
			cp.setAcountStructure(driver);
			waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));*/	
			
			cp.navigateToAccesstestingPortal(driver,cmnyID);			
			
			Set<String> handles =driver.getWindowHandles();			
			handles.remove(driver.getWindowHandle());			
			String handle = handles.iterator().next();			
			driver.switchTo().window(handle);
			System.out.println(driver.getCurrentUrl());
			waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));	
			
			
			hr.navigateToFileUpload(driver);
			hr.exportPopulation(driver, file, cmnyID);
			file.updateFile(cmnyID);
			hr.fileUpload(driver,hr.filePath);	
			
			waitForElementToDisplay(By.xpath("//span[contains(text(),'Upload & Validation Success!')]"));
			System.out.println(hr.validationSuccess.getText());
			
			Connection mycon= DatabaseUtils.getConnection("oracle.jdbc.driver.OracleDriver", 
					  "jdbc:oracle:thin:@ldap://oid.mercerhrs.com:389/bwqa14,cn=OracleContext,dc=world","Z_MM_MT1_A","MM_MT1_A_zqaFri2019");
			
			
			ResultSet query5= getResultSet(mycon, "select t.*, rowid from employee_life_events t where t.ssn in (SELECT SSN FROM COMP_SSN WHERE COMPANY_ID IN ('"+companyId+"'))");
			//metaData = query5.getMetaData();
						
			
			while(query5.next()) {
			    String lifeEventID = (query5.getString(4));
			    String lifeEventStatus = (query5.getString(11));
			    if(lifeEventID.equalsIgnoreCase("52")) {
			    	verifyEquals(lifeEventStatus, "C", test);					    	
			    }else if (lifeEventID.equalsIgnoreCase("55")) {
			    	verifyEquals(lifeEventStatus, "P", test);					    	
				}else if (lifeEventID.equalsIgnoreCase("55")) {
			    	verifyEquals(lifeEventStatus, "I", test);	
		        System.out.println(lifeEventID+" "+lifeEventStatus);			 
			    }
			}
			
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		}finally{
			reports.flush();
			driver.quit();
		}
		
	}	
}
