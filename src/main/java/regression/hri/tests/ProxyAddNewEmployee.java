package regression.hri.tests;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.waitForElementToDisappear;
import static driverfactory.Driver.waitForElementToDisplay;
import static utilities.DatabaseUtils.getResultSet;
import static utilities.MyExtentReports.reports;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;
import com.aventstack.extentreports.ExtentTest;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.hri.ConsultantPortal;
import pages.hri.EmployeeDetailsPage;
import pages.hri.HrAdminPage;
import pages.hri.LoginPage;
import utilities.DatabaseUtils;
import utilities.InitTests;
import verify.SoftAssertions;

import static verify.SoftAssertions.assertNotNull;
import static verify.SoftAssertions.verifyEquals;
import static  verify.SoftAssertions.verifyEqualsIgnoreCase;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class ProxyAddNewEmployee extends InitTests {

	WebDriver driver=null;
	WebDriver webdriver = null;
	Driver driverFact = new Driver();
	ExtentTest test=null;
	String companyId;
	
	public ProxyAddNewEmployee(String appName) {
		super(appName);	
	}
	
	
	@Test(enabled=true , priority= 11)
	public void verifyHRAdminFlow() throws Exception	{		
		new ProxyAddNewEmployee("HRI");			
		try {
			webdriver = driverFact.initWebDriver(BASEURL,BROWSER_TYPE,"local","");
			test = reports.createTest("Verify Adding new employee Via Access Testing portal ");
			test.assignCategory("Regression");
			driver=driverFact.getEventDriver(webdriver,test);
			
			LoginPage login =  new LoginPage(driver);			
			login.login(USERNAME,PASSWORD);
			
			
			
			ConsultantPortal cp = new ConsultantPortal(driver);
			cp.navigateToAccesstestingPortal(driver,"39096");
			
			
			Set<String> handles =driver.getWindowHandles();			
			handles.remove(driver.getWindowHandle());			
			String handle = handles.iterator().next();			     
			driver.switchTo().window(handle);
			System.out.println(driver.getCurrentUrl());
			waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));	
			
			
			HrAdminPage hr = new HrAdminPage(driver);
			hr.addNewEmployee(driver);
			
			EmployeeDetailsPage emp = new EmployeeDetailsPage(driver);
			emp.addNewProfileDetails(driver);
			
			
			Connection mycon= DatabaseUtils.getConnection("oracle.jdbc.driver.OracleDriver", 
					  "jdbc:oracle:thin:@ldap://oid.mercerhrs.com:389/bwqa14,cn=OracleContext,dc=world","Z_MM_MT1_A","MM_MT1_A_zqaFri2019");
			 
			String real_ssn = emp.randSSN;
			System.out.println("Running query1...");
			ResultSet query1= getResultSet(mycon, "SELECT * FROM COMP_SSN where ssn in (select ssn from comp_ssn where real_ssn ='"+real_ssn +"')");
			query1.next();
			String ssnQuery1 = query1.getString(2);
		//	System.out.println(query1.getString(2));

			System.out.println("Running query2...");
			ResultSet query2= getResultSet(mycon, "select t.*, rowid from employee_eff_date t where t.ssn in (select ssn from comp_ssn where real_ssn ='"+real_ssn +"')");
			query2.next();
			String ssnQuery2 = query2.getString(2);
			String fName = query2.getString(4);
			String lName = query2.getString(5);
			String Email = query2.getString(26);
			String workEmail = query2.getString(27);
			System.out.println(workEmail);
			
			verifyEquals(ssnQuery1, ssnQuery2, test);
			verifyEquals(fName, emp.firstname, test);
			verifyEquals(lName, emp.lastname, test);
			verifyEquals(Email, emp.email, test);
			verifyEquals(workEmail, emp.wemail, test);			
			
			
			
			
			
			
			
			
			
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		}finally{
			reports.flush();
			driver.quit();
		}
		
	}	
}
