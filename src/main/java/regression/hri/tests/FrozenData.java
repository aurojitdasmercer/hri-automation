package regression.hri.tests;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.waitForElementToDisappear;
import static driverfactory.Driver.waitForElementToDisplay;
import static utilities.DatabaseUtils.getResultSet;
import static utilities.MyExtentReports.reports;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;
import com.aventstack.extentreports.ExtentTest;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.hri.ConsultantPortal;
import pages.hri.LoginPage;
import utilities.DatabaseUtils;
import utilities.InitTests;
import verify.SoftAssertions;

import static verify.SoftAssertions.assertNotNull;
import static verify.SoftAssertions.verifyEquals;
import static  verify.SoftAssertions.verifyEqualsIgnoreCase;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.List;

public class FrozenData extends InitTests {

	WebDriver driver=null;
	WebDriver webdriver = null;
	Driver driverFact = new Driver();
	ExtentTest test=null;
	String companyId;
	
	public FrozenData(String appName) {
		super(appName);	
	}
	
	
	@Test(enabled=true , priority= 1)
	public void verifyFrozenDataSalaryConfiguration() throws Exception	{		
		new FrozenData("HRI");			
		try {
			webdriver = driverFact.initWebDriver(BASEURL,BROWSER_TYPE,"local","");
			test = reports.createTest("Verify Frozen Data ");
			test.assignCategory("Regression");
			driver=driverFact.getEventDriver(webdriver,test);
			
			LoginPage login =  new LoginPage(driver);
			login.login(USERNAME,PASSWORD);
			
			FileOperations file = new FileOperations();
			ConsultantPortal cp = new ConsultantPortal(driver);
						
			cp.addNewClient(driver);			
			cp.createClientProfile(driver,"Enhanced","Y");
			cp.selectPlanDescisions(driver);
			
			cp.selectBenifits(driver,file);
			
			file.updateFile();
			cp.electionMappingUpload.sendKeys(file.filePath);
			clickElement(cp.electionMappingUploadButton);
			clickElement(cp.electionMappingContinue);
			waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));
			
			cp.selectContributionStrategy(driver);
			
			cp.selectRates(driver);		
					
			System.out.println("RatesM");
			
			cp.setAdministration(driver);
			
			companyId=  cp.cmpnyId;
			
					
			clickElement(cp.demographicUploadNext2);				
			waitForElementToDisplay(By.cssSelector("div[class='processing-request']"));
			waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));
			clickElement(cp.automatedLoadSaveandContinue);
			waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));
			
			cp.setAcountStructure(driver);
			
			//companyId="38224";			
	
			Thread.sleep(3000);
			driver.get("https://consultant-qaf.mercermarketplace365plus.com/#/pendingClients/wizard/"+companyId+"/2020/administrationSalaryRules");
			
			clickElement(cp.baseSalary);
			clickElement(cp.frozendataSavendContinue);
			waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));
			
			  List<String>Ssn = new ArrayList<String>();
			  List<String>frozenAmtBase  = new ArrayList<String>();				  
			  List<String>frozenData = new ArrayList<String>();
			  List<String>benifitSal = new ArrayList<String>();
			  List<String>currentSal = new ArrayList<String>();
			  List<String>frozenAmtBenifits  = new ArrayList<String>();
			

			Connection mycon= DatabaseUtils.getConnection("oracle.jdbc.driver.OracleDriver", 
					  "jdbc:oracle:thin:@ldap://oid.mercerhrs.com:389/bwqa14,cn=OracleContext,dc=world","Z_MM_MT1_A","MM_MT1_A_zqaFri2019");
			file.readEmployeeFile();
			  		
					System.out.println("Running query1...");
					ResultSet query1= getResultSet(mycon, "select * from employee_frozen_data t where t.ssn in (select ssn from COMP_SSN where company_id in  ('"+companyId+"'))");
					ResultSet query3 = getResultSet(mycon, "select * from sch_sal_svc t where t.ssn in (select ssn from COMP_SSN where company_id in ('"+companyId+"'))");
				  			  
				  while(query1.next()) {
					   	Ssn.add((query1.getString(9)));
					   	frozenAmtBase.add((query1.getString(10)));
					   	System.out.println(query1.getString(9)+" "+query1.getString(10));
					   	frozenData.add((query1.getString(11)));
					    }
				
					for(int j = 8;j<file.maxRowEmp;j++){						
						benifitSal.add(file.sheet_obj_EmpFile.getRow(j).getCell(35).getStringCellValue());
						currentSal.add(file.sheet_obj_EmpFile.getRow(j).getCell(34).getStringCellValue());
						
					}
									  
					//First Query VAlidation
				  for(int i=0;i<currentSal.size();i++) {
					verifyEquals(frozenAmtBase.get(i),currentSal.get(i) , test);						 
				  }
				  
				  for(int i=0;i<currentSal.size();i++) {
					  assertNotNull(frozenData.get(i), " CheckingFrozenDataIsNotNull", test);
					 
				  }
				  
				  for(int i=0;i<currentSal.size();i++) {
					  assertNotNull(Ssn.get(i), " CheckingFrozenDataIsNotNull", test);
				  }
				  
				  
				 
				  
				  driver.get("https://consultant-qaf.mercermarketplace365plus.com/#/pendingClients/wizard/"+companyId+"/2020/administrationSalaryRules");
					
					clickElement(cp.benifitsSalary);
					clickElement(cp.frozendataSavendContinue);
					waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));
					Thread.sleep(5000);
					System.out.println("Running query2...");
					ResultSet query2= getResultSet(mycon, "select * from employee_frozen_data t where t.ssn in (select ssn from COMP_SSN where company_id in  ('"+companyId+"'))");
					ResultSet query4 = getResultSet(mycon, "select * from sch_sal_svc t where t.ssn in (select ssn from COMP_SSN where company_id in ('"+companyId+"'))");
					  
					
					while(query2.next()) {					   	
					   	frozenAmtBenifits.add((query2.getString(10)));					   	
					    }
					
					for(int i=0;i<currentSal.size();i++) {
						  System.out.println(Ssn.get(i)+":"+frozenAmtBenifits.get(i)+":"+frozenData.get(i));	
						  verifyEquals(frozenAmtBenifits.get(i),benifitSal.get(i) , test);	
					  }
					
					//Second Query Validation
					int counter=0;
					
					
					System.out.println("Current Sal..");
					for(int i=0;i<currentSal.size();i++) {
						query3.next();
						verifyEquals(currentSal.get(i),query3.getString(41) , test);	
						System.out.println(currentSal.get(i)+" "+query3.getString(41));
						
					}
					
					counter=0;
					
					
					System.out.println("Benifit Sal..");
					for(int i=0;i<currentSal.size();i++) {
						query4.next();
						verifyEquals(benifitSal.get(i),query4.getString(49) , test);
						System.out.println(benifitSal.get(i)+" "+query4.getString(49));
					}
					 
					  
					
					
					
			
					
				  
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		}finally{
			reports.flush();
			driver.close();
		}
		
	}
}
