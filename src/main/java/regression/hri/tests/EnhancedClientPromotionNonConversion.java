package regression.hri.tests;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.waitForElementToDisappear;
import static driverfactory.Driver.waitForElementToDisplay;
import static utilities.DatabaseUtils.getResultSet;
import static utilities.MyExtentReports.reports;

import org.apache.poi.ss.usermodel.DataFormatter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;
import com.aventstack.extentreports.ExtentTest;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.hri.ConsultantPortal;
import pages.hri.LoginPage;
import utilities.DatabaseUtils;
import utilities.InitTests;
import verify.SoftAssertions;

import static verify.SoftAssertions.assertNotNull;
import static verify.SoftAssertions.verifyEquals;
import static  verify.SoftAssertions.verifyEqualsIgnoreCase;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.List;

public class EnhancedClientPromotionNonConversion extends InitTests {

	WebDriver driver=null;
	WebDriver webdriver = null;
	Driver driverFact = new Driver();
	ExtentTest test=null;
	String companyId;
	
	public EnhancedClientPromotionNonConversion(String appName) {
		super(appName);	
	}
	
	
	@Test(enabled=true , priority= 7)
	public void verifyEnhancedPromotion() throws Exception	{		
		new EnhancedClientPromotionNonConversion("HRI");			
		try {
			webdriver = driverFact.initWebDriver(BASEURL,BROWSER_TYPE,"local","");
			test = reports.createTest("Verify Enhanced Client Promotion NonConversion ");
			test.assignCategory("Regression");
			driver=driverFact.getEventDriver(webdriver,test);
			
			LoginPage login =  new LoginPage(driver);
			login.login(USERNAME,PASSWORD);
			
			
			ConsultantPortal cp = new ConsultantPortal(driver);	
			FileOperations file = new FileOperations();
			cp.addNewClient(driver);			
			cp.createClientProfile(driver,"Enhanced","N");			
			cp.selectPlanDescisions(driver);
			
			cp.selectBenifits(driver,file);			
			cp.selectContributionStrategy(driver);
			cp.selectRates(driver);		
			cp.setAdministration(driver);
			
			
			companyId=  cp.cmpnyId;
			
					
					clickElement(cp.demographicUploadNext2);				
					waitForElementToDisplay(By.cssSelector("div[class='processing-request']"));
					waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));
					clickElement(cp.automatedLoadSaveandContinue);
					waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));
					
					cp.setAcountStructure(driver);
					
					Connection mycon= DatabaseUtils.getConnection("oracle.jdbc.driver.OracleDriver", 
							  "jdbc:oracle:thin:@ldap://oid.mercerhrs.com:389/bwqa14,cn=OracleContext,dc=world","Z_MM_MT1_A","MM_MT1_A_zqaFri2019");
					 
					System.out.println("Running query1...");
					ResultSet query1= getResultSet(mycon, "SELECT * FROM COMP_SSN WHERE COMPANY_ID IN ('"+companyId+"')");
					file.readEmployeeFile();
					List<String> SSN = new ArrayList<String>();
					ResultSetMetaData metaData = query1.getMetaData();
					int columnCount = metaData.getColumnCount();				
					System.out.println(columnCount+" ");
					int i = 8;
					
					while(query1.next()) {
					 	String realSsn = (query1.getString(4));
					   	String ExcelSsn = file.sheet_obj_EmpFile.getRow(i).getCell(1).getStringCellValue();
					    verifyEquals(realSsn, ExcelSsn, test);
					   	i++;
					   	SSN.add(query1.getString(2));		 
					    //System.out.println(realSsn+" "+ExcelSsn);		
					   }
					
				System.out.println("Running Query2...");  	
				ResultSet query2= getResultSet(mycon, "select * from company_employment_data WHERE COMPANY_ID IN ('"+companyId+"')");
							  			
				List<String> ssn_query2 = new ArrayList<String>();
				List<String> jobClassCode_query = new ArrayList<String>();
				List<String> salaryHrlyCode_Query = new ArrayList<String>();
				List<String> payStatusCode_Query = new ArrayList<String>();
				List<String> workCode_Query = new ArrayList<String>();
				List<String> deptCode_Query = new ArrayList<String>();
				
				//Getting data from DB
				while(query2.next()) {
					ssn_query2.add(query2.getString(2));
					jobClassCode_query.add(query2.getString(5));
					salaryHrlyCode_Query.add(query2.getString(7));
					payStatusCode_Query.add(query2.getString(6));
					workCode_Query.add(query2.getString(9));
					deptCode_Query.add(query2.getString(11));				
				}
				
				DataFormatter format = new DataFormatter();
				
				List<String> jobClassCode_excel = new ArrayList<String>();
				List<String> salaryHrlyCode_excel = new ArrayList<String>();
				List<String> payStatusCode_excel = new ArrayList<String>();
				List<String> workCode_excel = new ArrayList<String>();
				List<String> deptCode_excel = new ArrayList<String>();
				
				//getting data from excel
				for(i=8;i<=file.sheet_obj_EmpFile.getLastRowNum();i++) {
					jobClassCode_excel.add(format.formatCellValue(file.sheet_obj_EmpFile.getRow(i).getCell(31)));
					salaryHrlyCode_excel.add(format.formatCellValue(file.sheet_obj_EmpFile.getRow(i).getCell(37)));
					payStatusCode_excel.add(format.formatCellValue(file.sheet_obj_EmpFile.getRow(i).getCell(25)));
					workCode_excel.add(format.formatCellValue(file.sheet_obj_EmpFile.getRow(i).getCell(28)));
					deptCode_excel.add(format.formatCellValue(file.sheet_obj_EmpFile.getRow(i).getCell(30)));			
				}
				
				
			
				
				
				// Verify SSN
				for(int j = 0;j<SSN.size();j++) {
					verifyEquals(SSN.get(j), ssn_query2.get(j), test);
				}
				
				//Verify Job Class Code
				for(int j = 0;j<SSN.size();j++) {
					verifyEquals(jobClassCode_excel.get(j), jobClassCode_query.get(j), test);
				}
				
				//	Salary Hour code
				for(int j = 0;j<SSN.size();j++) {
					verifyEquals(salaryHrlyCode_excel.get(j), salaryHrlyCode_Query.get(j), test);
				}	
				
				//Verify payStaus Code
				for(int j = 0;j<SSN.size();j++) {
					verifyEquals(payStatusCode_excel.get(j), payStatusCode_Query.get(j), test);
				}
				
				// Verify Work Code
				for(int j = 0;j<SSN.size();j++) {
					verifyEquals(workCode_excel.get(j), workCode_Query.get(j), test);
				}
				
				//Verify Dept Code
				for(int j = 0;j<SSN.size();j++) {
					verifyEquals(deptCode_excel.get(j), deptCode_Query.get(j), test);
				}
						
			
			
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		}finally{
			reports.flush();
			driver.close();
		}
		
	}

}
