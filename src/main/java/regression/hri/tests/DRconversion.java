package regression.hri.tests;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.waitForElementToDisappear;
import static driverfactory.Driver.waitForElementToDisplay;
import static utilities.DatabaseUtils.getResultSet;
import static utilities.MyExtentReports.reports;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;
import com.aventstack.extentreports.ExtentTest;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.hri.ConsultantPortal;
import pages.hri.LoginPage;
import utilities.DatabaseUtils;
import utilities.InitTests;
import verify.SoftAssertions;

import static verify.SoftAssertions.assertNotNull;
import static verify.SoftAssertions.verifyEquals;
import static  verify.SoftAssertions.verifyEqualsIgnoreCase;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;

public class DRconversion extends InitTests {

	WebDriver driver=null;
	WebDriver webdriver = null;
	Driver driverFact = new Driver();
	ExtentTest test=null;
	String companyId;
	
	public DRconversion(String appName) {
		super(appName);	
	}
	
	
	@Test(enabled=true , priority= 2)
	public void verifyDRconversionElectionMapping() throws Exception	{		
		new DRconversion("HRI");			
		try {
			webdriver = driverFact.initWebDriver(BASEURL,BROWSER_TYPE,"local","");
			test = reports.createTest("Verify DR Conversion ");
			test.assignCategory("Regression");
			driver=driverFact.getEventDriver(webdriver,test);
			
			LoginPage login =  new LoginPage(driver);
			login.login(USERNAME,PASSWORD);
			
			ConsultantPortal cp = new ConsultantPortal(driver);			
			cp.addNewClient(driver);			
			cp.createClientProfile(driver,"Enhanced","Y");
			cp.selectPlanDescisions(driver);
			FileOperations file = new FileOperations();
			cp.selectBenifits(driver,file);
			
			String actual;
			String expected;
			for(int i =0;i<file.maxCell-1;i++ ) {
				actual = file.sheet_obj.getRow(0).getCell(i).getStringCellValue();
				expected = file.defaultSheet.getRow(0).getCell(i).getStringCellValue();
				verifyEqualsIgnoreCase(actual, expected, test);
				if(actual.equalsIgnoreCase(expected)) {
					System.out.println(actual +"= "+ expected);
				}				
			}
			file.updateFile();
			cp.electionMappingUpload.sendKeys(file.filePath);
			clickElement(cp.electionMappingUploadButton);
			clickElement(cp.electionMappingContinue);
			waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));
			
			cp.selectContributionStrategy(driver);
			cp.selectRates(driver);		
			cp.setAdministration(driver);
			
			companyId=  cp.cmpnyId;
			
					
					clickElement(cp.demographicUploadNext2);				
					waitForElementToDisplay(By.cssSelector("div[class='processing-request']"));
					waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));
					clickElement(cp.automatedLoadSaveandContinue);
					waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));
					
					cp.setAcountStructure(driver);
					
					Connection mycon= DatabaseUtils.getConnection("oracle.jdbc.driver.OracleDriver", 
							  "jdbc:oracle:thin:@ldap://oid.mercerhrs.com:389/bwqa14,cn=OracleContext,dc=world","Z_MM_MT1_A","MM_MT1_A_zqaFri2019");
					 
					  		System.out.println("Running query1...");
						  ResultSet query1= getResultSet(mycon, "SELECT * FROM COMP_SSN WHERE COMPANY_ID IN ('"+companyId+"')");
						  
						  ResultSetMetaData metaData = query1.getMetaData();
							int columnCount = metaData.getColumnCount();				
							System.out.println(columnCount+" ");
							int i = 8;
							file.readEmployeeFile();
							while(query1.next()) {
							   	String realSsn = (query1.getString(4));
							   	String ExcelSsn = file.sheet_obj_EmpFile.getRow(i).getCell(1).getStringCellValue();
							   verifyEquals(realSsn, ExcelSsn, test);
							   	i++;
						        System.out.println(realSsn);			 
							    }
							
							System.out.println("Running query2...");
							
							ResultSet query2= getResultSet(mycon, "select * from dep_ssn t where t.ssn in (SELECT SSN FROM COMP_SSN WHERE COMPANY_ID IN ('"+companyId+"'))");
							metaData = query2.getMetaData();
										
							System.out.println(columnCount+" ");
							while(query2.next()) {
							    String depSsn = (query2.getString(3));
							    assertNotNull(depSsn, "Checking Dep_ ssn", test);
						        System.out.println(depSsn);			 
							    }
							
							System.out.println("Running query3...");
							
							ResultSet query3= getResultSet(mycon, "select t.*, rowid from employee_eff_date t where t.ssn in (SELECT SSN FROM COMP_SSN WHERE COMPANY_ID IN ('"+companyId+"'))");
							metaData = query3.getMetaData();
										
							System.out.println(columnCount+" ");
							while(query3.next()) {
							    String effDate = (query3.getString(3));
							    assertNotNull(effDate, " Checking effDate", test);
						        System.out.println(effDate);			 
							    }
							
							System.out.println("Running query4...");
							
							ResultSet query4= getResultSet(mycon, "select t.*, rowid from dependent_eff_date t where t.ssn in (SELECT SSN FROM COMP_SSN WHERE COMPANY_ID IN ('"+companyId+"'))");
							metaData = query4.getMetaData();
										
							System.out.println(columnCount+" ");
							while(query4.next()) {
							    String dep_effDate = (query4.getString(4));
							    assertNotNull(dep_effDate, " Checking dep_effDate", test);
						        System.out.println(dep_effDate);			 
							    }
							
							System.out.println("Running query5...");
							
							ResultSet query5= getResultSet(mycon, "select t.*, rowid from employee_life_events t where t.ssn in (SELECT SSN FROM COMP_SSN WHERE COMPANY_ID IN ('"+companyId+"'))");
							metaData = query5.getMetaData();
										
							System.out.println(columnCount+" ");
							while(query5.next()) {
							    String lifeEventID = (query5.getString(4));
							    String lifeEventStatus = (query5.getString(11));
							    if(lifeEventID.equalsIgnoreCase("52")) {
							    	verifyEquals(lifeEventStatus, "C", test);					    	
							    }else if (lifeEventID.equalsIgnoreCase("55")) {
							    	verifyEquals(lifeEventStatus, "P", test);					    	
								}
						        System.out.println(lifeEventID+" "+lifeEventStatus);			 
							    }
			
			
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		}finally{
			reports.flush();
			driver.close();
		}
		
	}

}
