package regression.hri.tests;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.waitForElementToDisappear;
import static utilities.MyExtentReports.reports;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;
import com.aventstack.extentreports.ExtentTest;
import atu.testng.reports.ATUReports;
import atu.testng.reports.logging.LogAs;
import atu.testng.selenium.reports.CaptureScreen;
import atu.testng.selenium.reports.CaptureScreen.ScreenshotOf;
import driverfactory.Driver;
import pages.hri.ConsultantPortal;
import pages.hri.LoginPage;
import pages.hri.dbTest;
import utilities.InitTests;
import verify.SoftAssertions;
import static  verify.SoftAssertions.verifyEqualsIgnoreCase;

public class DRconversion2 extends InitTests {

	WebDriver driver=null;
	WebDriver webdriver = null;
	Driver driverFact = new Driver();
	ExtentTest test=null;
	
	public DRconversion2(String appName) {
		super(appName);	
	}
	
	
	@Test(enabled=false , priority= 1)
	public void verifyDRconversion() throws Exception	{		
		new DRconversion2("HRI");			
		try {
			webdriver = driverFact.initWebDriver(BASEURL,BROWSER_TYPE,"local","");
			test = reports.createTest("Verify Sitecore "+System.getProperty("env")+" is up ");
			test.assignCategory("Regression");
			driver=driverFact.getEventDriver(webdriver,test);
			
			dbTest db = new dbTest();
			db.ScheduleListDB("test",test);
			
			
			/*int i;
			
			WebElement username;
			WebElement submit;
			WebElement PASS; 
			WebElement submitpas;
			WebElement oldpas;
			WebElement newpas;
			WebElement retypepas;
			WebElement restPas;
			
			for(i=122;i<=1500;i++) {
			try {
				
				driver.get("https://stg-harmonise.mercer.com/provision/login ");
				Thread.sleep(3000);
				 username = driver.findElement(By.xpath("//*[@id=\"login-form_HyKfbbgVFjxZ\"]"));
				 if(i<10) {
					 username.sendKeys("perftest000"+i+".eptest.admin2@gisqa.mercer.com");
				 }else if(i<100){
					 username.sendKeys("perftest00"+i+".eptest.admin2@gisqa.mercer.com");
				 }else if(i<1000){
					 username.sendKeys("perftest0"+i+".eptest.admin2@gisqa.mercer.com");
				 }else {
					 username.sendKeys("perftest"+i+".eptest.admin2@gisqa.mercer.com");
				 }
				
				 submit = driver.findElement(By.xpath("//*[@id=\"login-form_rkcM-WxNtoeW\"]"));
				submit.click();
				Thread.sleep(7000);
			
				 PASS = driver.findElement(By.xpath("//*[@id=\"login-form_SyifZZxEKjxZ\"]"));
				PASS.sendKeys("Welcome3");			
				 submitpas = driver.findElement(By.xpath("//*[@id=\"login-form_SJbmZWlVtjeW\"]"));
				submitpas.click();
				Thread.sleep(6000);
				 oldpas = driver.findElement(By.xpath("//*[@id=\"password-expired-form_rkQ7ZWlVtieb\"]"));
				oldpas.sendKeys("Welcome3");
				 newpas = driver.findElement(By.xpath("//*[@id=\"password-expired-form_rkEXWWeNtsl-\"]"));
				newpas.sendKeys("Welcome4");
				 retypepas = driver.findElement(By.xpath("//*[@id=\"password-expired-form_H1HQ-WlNYixW\"]"));
				retypepas.sendKeys("Welcome4");
				 restPas = driver.findElement(By.xpath("//*[@id=\"password-expired-form_rkP7bZgNYigb\"]"));
				restPas.click();
				Thread.sleep(3000);
				System.out.println(i+" changed");
			
				
			} catch (Exception e) {
				
				System.out.println(i+"    ---->Failed");
				}
			
			
			
			
			}*/
			/*WebElement oldpas = driver.findElement(By.xpath("//*[@id=\"password-expired-form_rkQ7ZWlVtieb\"]"));
			WebElement newpas = driver.findElement(By.xpath("//*[@id=\"password-expired-form_rkEXWWeNtsl-\"]"));
			WebElement retypepas = driver.findElement(By.xpath("//*[@id=\"password-expired-form_H1HQ-WlNYixW\"]"));
			WebElement restPas = driver.findElement(By.xpath("//*[@id=\"password-expired-form_rkP7bZgNYigb\"]"));*/
			
			
			
		} catch (Error e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("testSearch()", LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		} catch (Exception e) {
			e.printStackTrace();
			SoftAssertions.fail(e, driverFact.getScreenPath(driver,new Exception().getStackTrace()[0].getMethodName()), test);
			ATUReports.add("testSearch()", e.getMessage(), LogAs.FAILED, new CaptureScreen(ScreenshotOf.DESKTOP));
			softAssert.assertAll();
		}finally{
			reports.flush();
			//driver.close();
		}
		
	}
	

}
