package pages.belongng;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class KindredLoginPage {
	
WebDriver driver;
	
	@FindBy(css="img[class='auth-org-logo']")
	public  WebElement kindredLogo;
	
	@FindBy(id = "lblInstruction")
	public WebElement instructionLabel;
	
	@FindBy(xpath = "//label[text()='Remember me']")
	public WebElement remmberMe;
	
	@FindBy(xpath = "//h2[text()='Sign In']")
	public WebElement signInHeader;
	
	@FindBy(xpath = "//h3[text()='Welcome to Kindred For Me!']")
	public WebElement welcomeHeader;	
	
	@FindBy(id = "okta-signin-username")
	public  WebElement oktaUserName;
	
	@FindBy(id = "okta-signin-password")
	public  WebElement oktaPassword;
	
	@FindBy(id = "okta-signin-submit")
	public  WebElement oktaSignin;

	@FindBy(id = "Credentials_0__CredentialValue")
	public  WebElement userName;

	@FindBy(css = "label[for='Credentials_0__CredentialValue']")
	public  WebElement userNameText;

	@FindBy(id = "Credentials_1__CredentialValue")
	public  WebElement password;

	@FindBy(css = "label[for='Credentials_1__CredentialValue']")
	public  WebElement pwdText;

	@FindBy(css = "input[value='Login']")
	public  WebElement loginBtn;
	
	public KindredLoginPage(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}

}
