package pages.belongng;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.waitForElementToDisplay;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ImpersonateRolesPage {

	@FindBy(css = "a[href='/ImpersonateRoles']")
	public static WebElement impersonRoles;

	@FindBy(xpath = "//h1[contains(text(),'Impersonate Roles')]")
	public static WebElement impersonRolesHeading;
	// (//input[@value='Impersonate'])[1]

	@FindBy(css = "span[class='message danger']")
	public static List<WebElement> currentRole;

	@FindBy(id = "btnChangeRoles")
	public static WebElement changeRoleBtn;

	@FindBy(css = "div#tabs a")
	//div[@id='tabs']//a
	public static List<WebElement> availableTags;

	WebDriver driver;

	//@FindBy(xpath="(//div[@id='tabs']//a)[1]/ancestor::div[@id='tabs']//span[@title='CanonUSA']")
	//div[@id='tabs']//a[text()='Division']/ancestor::div[@id='tabs']//span[@title='CanonUSA']
	
	public ImpersonateRolesPage(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	public void select(WebElement ele) throws InterruptedException {
			clickElement(ele);
	}
	
	public void selectSpecifiedRoles(String selectTag, String selectRole,WebDriver driver) throws InterruptedException {
		String strignToSelectRole = "//div[@id='tabs']//a[text()='" + selectTag
				+ "']/ancestor::div[@id='tabs']//span[@title='" + selectRole + "']/input";
		waitForElementToDisplay(driver.findElement(By.xpath(strignToSelectRole)));
		WebElement xpathToSelectRole = driver.findElement(By.xpath(strignToSelectRole));
		select(xpathToSelectRole);
		}
}
