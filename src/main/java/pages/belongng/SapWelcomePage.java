package pages.belongng;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.scrollToElement;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SapWelcomePage {
	WebDriver driver;
	
	public SapWelcomePage(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	//@FindBy(xpath="//span[contains(text(),'select a country')]")
	@FindBy(id="filter_tag-CountrySelectBoxItText")	
	public  WebElement selectCountry;
	
	
	@FindBy(xpath="//span[text()='North America Benefits']")	
	public  WebElement northAmericaBenifits;
	
	
	
	@FindBy(id="rememberme")	
	public  WebElement rememberMe;
	
	//	@FindBy(xpath="//span[text()='Canada']")
	@FindBy(linkText="Canada")
	public  WebElement selectCanada;
	
	
	//@FindBy(xpath="//span[contains(text(),'United States')]")
	@FindBy(linkText="United States")
	public  WebElement selectUnitedStates;
	
	@FindBy(css = "input[value='Login']")
	public  WebElement loginBtn;
	
	@FindBy(xpath="//p[contains(text(),'Prospective employees')]/following-sibling::div//input[@value='Enter']")
	public  WebElement prospectiveEmployeesLogin;
	
	@FindBy(xpath="//p[contains(text(),'No longer an NRG employee')]/following-sibling::div//input[@value='Enter']")
	public  WebElement notNRGEmployeeLogin;
	
	@FindBy(xpath="//span[text()='Sign Me In']")
	public  WebElement loginNRGSSO;
	
	public void scrollAndClick(WebDriver driver,WebElement element) throws InterruptedException {
		scrollToElement(driver,element);
		clickElement(element);
	}
}
