package pages.belongng;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.hoverAndClickOnElement;
import static driverfactory.Driver.scrollToElement;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SelectClientPage {

	WebDriver driver;
	String clientName;

	WebElement selectClient;

	@FindBy(id = "btnSelectClient")
	public static WebElement selectClientBtn;

	@FindBy(css = "option[selected='selected']")
	public static WebElement selectedOption;

	@FindBy(xpath = "//h3[text()='Select a Client']")
	// div[@id='modal_SelectClient']//h3[@id='versionLabel']")
	public static WebElement selectClientText;

	@FindBy(id = "NavigationClientDDL")
	public static WebElement clientDropDown;

	@FindBy(id = "//div[@id='default']//h1")
	public static WebElement MercerBelongAdminTxt;

	public SelectClientPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public void selectClient(String clientName) throws InterruptedException {
		WebElement selectClient = driver.findElement(By.xpath("//div[@id='modal_SelectClient']//div[@class='modal-body']//a[text()='" + clientName + "']"));
		hoverAndClickOnElement(driver, selectClient, selectClient);
		scrollToElement(driver, selectClientBtn);
		clickElement(selectClientBtn);
	}

}
