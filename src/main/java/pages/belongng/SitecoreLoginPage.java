package pages.belongng;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SitecoreLoginPage {
	
	WebDriver driver;
	
	@FindBy(name="UserName")
	public  WebElement username;
	
	@FindBy(id="loginLbl")
	public  WebElement usernameLabel;
	
	@FindBy(id="Password")
	public  WebElement password;
	
	@FindBy(id="passLabel")
	public  WebElement passwordLabel;
	
	
	@FindBy(id="LogInBtn")
	public  WebElement loginbutton;
	
	public SitecoreLoginPage(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}		
	
}
