package pages.belongng;

import static driverfactory.Driver.scrollToElement;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ClientSiteHomePage extends HeadAdminClientBasePage {

	WebDriver driver;

	/*@FindBy(css="div[class='search-results']")
	public static WebElement searchResults;
	 */

	@FindBy(css="div#cta-default div.slick-track>div")
	//xpath="//div[@id='cta-default']//div[@class='slick-track']/div"
	public static WebElement dashBoard;

	@FindBy(css="div#cta-default div.slick-track>div")
	//"//div[@id='cta-default']//div[@class='slick-track']/div"
	public static List<WebElement> dashBoards;

	@FindBy(css="div[class='column column-block']")
	public static List<WebElement> dashBoardsGilead;

	@FindBy(css="div[class='column column-block']")
	public static WebElement dashBoardGilead;

	@FindBy(xpath="//div[@id='cta-default']//div[@class='flipper flip-action']//div/img")
	public static WebElement dashBoard02;

	@FindBy(xpath="//div[@id='cta-default']//div[@class='flipper flip-action']//div/img")
	// div#cta-default div.flipper.flip-action div > img - nt working
	public static List<WebElement> dashBoards02;
	
	@FindBy(css="div#cta-default>div.text-component")
	//div[@id='cta-default']/div[@class='text-component']")
	public static WebElement dashBoardHumangood;
	
	@FindBy(css="div#cta-default>div.text-component")	
	//div[@id='cta-default']/div[@class='text-component']
	public static List<WebElement> dashBoardsHumangood;
	
	public ClientSiteHomePage(WebDriver driver) {
		super(driver);
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}

	 public  ArrayList<String> siteList() {
		 
		 ArrayList<String> siteList = new ArrayList<String>();
		 siteList.add("Getty");
		 siteList.add("Horizon");
		 siteList.add("AdvantageSolutions");
		 siteList.add("SLB");
		 siteList.add("WPP YR Recruit") ;
		 siteList.add("WPP Recruit");
		 siteList.add("WPP Ogilvy Recruit");
		 siteList.add("WPP Group M Recruit");
		 siteList.add("Genentech");
		 siteList.add("PlannedParenthood");
		 siteList.add("Nutanix") ;
		 siteList.add("TW");
		 siteList.add("WPP");
		 siteList.add("Stryker");
		 siteList.add("Crawford");
		 return siteList;
		 /*clientName.equalsIgnoreCase("Horizon") |
		  * clientName.equalsIgnoreCase("AdvantageSolutions") |
		  * clientName.equalsIgnoreCase("SLB") |
		  * clientName.equalsIgnoreCase("WPP YR Recruit") |
		  * clientName.equalsIgnoreCase("WPP Recruit") |
		  * clientName.equalsIgnoreCase("WPP Ogilvy Recruit") | 
		  * clientName.equalsIgnoreCase("WPP Group M Recruit") | 
		  * clientName.equalsIgnoreCase("Genentech")
			| clientName.equalsIgnoreCase("Getty") | 
			clientName.equalsIgnoreCase("PlannedParenthood")
			| clientName.equalsIgnoreCase("Nutanix") | clientName.equalsIgnoreCase("TW")
			| clientName.equalsIgnoreCase("Stryker") | clientName.equalsIgnoreCase("Wpp")*/
	 }
	

	
	public void scrollToDashBoard(String clientName) throws InterruptedException {
		if(clientName.equalsIgnoreCase("Gilead")) {
			scrollToElement(driver,dashBoardGilead);
		} else if (siteList().contains(clientName)) {
			scrollToElement(driver, dashBoard02);	
		}else if(clientName.equalsIgnoreCase("Humangood")) {
			scrollToElement(driver,dashBoardHumangood);
		}
		else {
			scrollToElement(driver,dashBoard);
		}

	}
	
	public int getDashBoardSize(String clientName) throws InterruptedException {
		scrollToDashBoard(clientName);
		if (clientName.equalsIgnoreCase("Gilead")) {
			return dashBoardsGilead.size();
		} else if (siteList().contains(clientName)) {
			return dashBoards02.size();
		} else if (clientName.equalsIgnoreCase("Humangood")) {
				return dashBoardsHumangood.size();
		} else {
			return dashBoards.size();
		}
	}
}
