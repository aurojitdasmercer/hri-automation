package pages.belongng;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import static driverfactory.Driver.setInput;
import static driverfactory.Driver.clickElement;
import org.openqa.selenium.support.PageFactory;

public class HumanresourcesHomepage {

	
	 WebDriver driver;
	
	public HumanresourcesHomepage(WebDriver driver){
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(css="li[class='search-icon']")
	public  WebElement searchIcon;

	@FindBy(id="searchBox")
	public  WebElement searchBox;
	
	@FindBy(id="searchBtn")
	public  WebElement searchButton;

	@FindBy(css="li[class*='menu-txt']")
	public  WebElement MenuIcon;

	@FindBy(css="div[class='impersonationSection']")
	public  WebElement impersonationSection;

	@FindBy(id="hd_7ffaff81-2e3c-47bd-a764-3198e8fb7976")
	public  WebElement notification;	

	@FindBy(xpath="//a[contains(text(),'Logout')]")
	public WebElement clientLogoutBtn;
	
	@FindBy(xpath="//*[@id='body-wrapper']/div[5]/div/div/div[2]/div[1]/div[1]")
	public WebElement searchResult;

	@FindBy(css="img[alt='close icon']")
	//img[@alt='close icon']
	public  WebElement closeMenuIcon;
	
	@FindBy(css="div.top-bar-left li.menu-text img")
	//div[@class='top-bar-left']//li[@class='menu-text']//img
	public  WebElement clientLogoInHeader;

		
	public void searchWord(String wordToSearch) throws InterruptedException {
		setInput(searchBox, wordToSearch);
		clickElement(searchButton);
		//searchBox.sendKeys(wordToSearch);
		//searchBox.sendKeys(Keys.ENTER);
		
	}
	
	
}
