package pages.belongng;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.scrollToElement;
import static driverfactory.Driver.waitForElementToDisplay;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FooterPage extends HeadAdminClientBasePage {

	static WebDriver driver;

	@FindBy(css = "span[class='footer-expand']")
	public static WebElement expandFooterIcon;
	//
	@FindBy(xpath = "(//div[@class='small-2 medium-2 large-2 columns float-right']//span)[2]")
	public static WebElement expandFooterIcon02;
	
	@FindBy(xpath = "//div[@class='row primary-footer']/div")
	public static WebElement topFooterItems;

	@FindBy(xpath = "//div[@class='row page-heading']//h1")
	public static WebElement pageHeading;

	@FindBy(css = "div[class*='primary-footer'] ul a")
	public static WebElement footerLnk;

	@FindBy(css = "div[class*='primary-footer'] ul a")
	public static List<WebElement> footerLnks;
	public WebElement eachMenuItem;

	public FooterPage(WebDriver driver) {
		super(driver);
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}

	public List<WebElement> getAllFooterItemsForHttp(String clientName)
			throws NullPointerException, IOException, URISyntaxException, InterruptedException {
		if(!clientName.equalsIgnoreCase("Rollins")) {
		scrollToElement(driver, expandFooterIcon);
		waitForElementToDisplay(expandFooterIcon);
		clickElement(expandFooterIcon);
		}
		else {
			scrollToElement(driver, expandFooterIcon);
			
			}
		waitForElementToDisplay(footerLnk);
		return footerLnks;
	}
}
