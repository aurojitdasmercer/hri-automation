package pages.belongng;

import static driverfactory.Driver.clickElement;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ManageClientUsersPage {
	// passing clientName,firstName
	WebDriver driver=null;
	
	@FindBy(id = "btnSearch")
	public static WebElement searchUsersBtn;

	@FindBy(xpath = "//a[contains(text(),' Users')]")
	public static WebElement manageUsers;
	
	@FindBy(css="a[href='/UserAdmin/GotoBelongHome']")
	public static WebElement toClientSite;
	
	@FindBy(css="a[href='/UserAdmin/GotoBelongHomePreview']")
	public static WebElement toClientPreviewSite;

	public static WebElement impersonateUser;

	public ManageClientUsersPage(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public void impersonatingUser(String firstName) throws InterruptedException {
		String impersonXpath = "//td[text()='" + firstName + "']/preceding-sibling::td/input[@value='Impersonate']";
		impersonateUser = driver.findElement(By.xpath(impersonXpath));
		clickElement(impersonateUser);
	}

}
