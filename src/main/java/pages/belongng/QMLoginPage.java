package pages.belongng;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.setInput;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class QMLoginPage {

	@FindBy(id = "Credentials_0__CredentialValue")
	public static WebElement userName;

	@FindBy(css = "label[for='Credentials_0__CredentialValue']")
	public static WebElement userNameText;

	@FindBy(id = "Credentials_1__CredentialValue")
	public static WebElement password;

	@FindBy(css = "label[for='Credentials_1__CredentialValue']")
	public static WebElement pwdText;

	@FindBy(css = "input[value='Login']")
	public static WebElement loginBtn;

	WebDriver driver;

	public QMLoginPage(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}

	public void login(String uName, String pwd) throws InterruptedException {
		setInput(userName, uName);
		setInput(password, pwd);
		clickElement(loginBtn);
	}

}
