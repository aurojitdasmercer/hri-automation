package pages.belongng;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HeadAdminClientBasePage {

	static WebDriver driver;
	
	public HeadAdminClientBasePage(WebDriver driver){
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(css="li[class='search-icon']")
	public static WebElement searchIcon;

	@FindBy(id="searchBox")
	public static WebElement searchBox;

	@FindBy(css="li[class*='menu-txt']")
	public static WebElement MenuIcon;

	@FindBy(css="div[class='impersonationSection']")
	public static WebElement impersonationSection;

	@FindBy(id="btn-admin")
	public static WebElement navigateFromClientToAdmin;

	@FindBy(css="div.top-bar-left li.menu-text")
	//div[@class='top-bar-left']//li[@class='menu-text']
	public static WebElement backToHomeIcon;

	@FindBy(xpath="//a[contains(text(),'Logout')]")
	public static WebElement clientLogoutBtn;

	@FindBy(css="img[alt='close icon']")
	//img[@alt='close icon']
	public static WebElement closeMenuIcon;
	
	@FindBy(css="div.top-bar-left li.menu-text img")
	//div[@class='top-bar-left']//li[@class='menu-text']//img
	public static WebElement clientLogoInHeader;

	@FindBy(xpath="//a[contains(text(),'Hi')]")
	public static WebElement HiInHeader;
	
	@FindBy(xpath="//button[contains(text(),'Hi')]")
	public static WebElement HiInHeaderForCBS;
	
	@FindBy(xpath="//div[@class='cbs-dropdown-header']/h3[text()='Employee Self Service']")
	public static WebElement CBSEmpServHeader;

	//a[text()='English']
	@FindBy(xpath="//a[text()='English']")
	public static WebElement SAPLangChange;
	
	//a[text()='Français']
	@FindBy(xpath="//a[text()='Français']")
	public static WebElement SAPFrenchLang;
	
	public void searchWord(String wordToSearch) {
		searchBox.sendKeys(wordToSearch);
		searchBox.sendKeys(Keys.ENTER);
	}
	
	
}
