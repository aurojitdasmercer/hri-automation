package pages.belongng;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class BelongAdminLoginPage {

	
	@FindBy(id = "Credentials_0__CredentialValue")
	public WebElement userName;

	@FindBy(css = "label[for='Credentials_0__CredentialValue']")
	public WebElement userNameText;
	
	@FindBy(id = "lblInstruction")
	public WebElement instructionLabel;
	
	
	@FindBy(xpath = "//h3[text()='Welcome to the Mercer Belong Administration Center']")
	public WebElement welcomeHeader;
	
	

	@FindBy(id = "Credentials_1__CredentialValue")
	public WebElement password;

	@FindBy(css = "label[for='Credentials_1__CredentialValue']")
	public WebElement pwdText;

	@FindBy(css = "input[value='Login']")
	public WebElement loginBtn;

	WebDriver driver;

	public BelongAdminLoginPage(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
}
