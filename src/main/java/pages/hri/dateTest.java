package pages.hri;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class dateTest {
	
	public static void main(String[] args) {
		Date currentDate = new Date();
		DateFormat  dateformat = new SimpleDateFormat("MM/dd/yyyy");
		DateFormat CurrentMonth =  new SimpleDateFormat("MM");
		DateFormat Currentyear =  new SimpleDateFormat("yyyy");
		int crntMnth = Integer.parseInt(CurrentMonth.format(currentDate));
		int crntYear = Integer.parseInt(Currentyear.format(currentDate)); 
		System.out.println(dateformat.format(currentDate));
		Calendar c = Calendar.getInstance();
        c.setTime(currentDate);
        c.add(Calendar.DATE, 7);
        Date openEnrollmentStartDate_date = c.getTime();
        System.out.println(dateformat.format(openEnrollmentStartDate_date));
        c.setTime(currentDate);
        c.add(Calendar.DATE, 14);
        Date openEnrollmentEndDate_date = c.getTime();
        System.out.println(dateformat.format(openEnrollmentEndDate_date));
        Date planYearEffectiveDate_date;        
        c.set(crntYear+1,crntMnth,1);
        c.set(Calendar.MONTH, 1);
        planYearEffectiveDate_date = c.getTime();
        System.out.println(dateformat.format(planYearEffectiveDate_date));
        
        
	}

}
