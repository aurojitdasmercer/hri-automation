package pages.hri;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import regression.hri.tests.FileOperations;
import utilities.SikuliActions;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.setInput;
import static driverfactory.Driver.waitForElementToDisappear;
import static driverfactory.Driver.waitForElementToDisplay;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static driverfactory.Driver.hoverAndClickOnElement;

public class HrAdminPage {
	WebDriver driver;
	
	public HrAdminPage(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath="//a[contains(text(),'Employees ')]")	
	  WebElement employeesDropDown;	
	
	@FindBy(css="a[ng-click='gotoEmployees()']")	
	public  WebElement viewEmployees;	
	
	@FindBy(xpath="(//a[@class='ng-binding'])[6]")	
	public  WebElement firstEmployee;	
	
	@FindBy(css="a[ui-sref='hr.newEmployee({mode: mode})']")	
	public  WebElement addNewEmployee;	
	
	@FindBy(css="a[ng-click='gotoUploadTestingEmp()']")	
	public  WebElement uploadPage;	
	
	@FindBy(id="fileupload")	
	public  WebElement choosedFileButton;
	
	@FindBy(css="a[ng-click='exportAllEmployees()']")	
	public  WebElement exportPopulation;
	
	@FindBy(css="div[class='button-lead-icon-box']")	
	public  WebElement uploadButton;
	
	@FindBy(xpath="//span[contains(text(),'Upload & Validation Success!')]")	
	public  WebElement validationSuccess;
	

	@FindBy(xpath="//span[contains(text(),'Errors')]")	
	public  WebElement errorTab;
	
	@FindBy(xpath="(//a[@data-tooltip='Fix Error'])[1]")	
	public  WebElement editEmployee1;
	
	
	@FindBy(xpath="(//a[@data-tooltip='Fix Error'])[2]")	
	public  WebElement editEmployee2;
	
	@FindBy(xpath="(//a[@data-tooltip='Fix Error'])[3]")	
	public  WebElement editEmployee3;
	
	@FindBy(css="a[ng-click='resubmit()']")	
	public  WebElement reSubmitButton;
	

	@FindBy(css="input[type='text']")	
	public  WebElement newValue;
	
	@FindBy(css="a[ng-click='save()']")	
	public  WebElement save;
	
	@FindBy(xpath="(//span[@class='number ng-binding'])[2]")	
	public  WebElement uploadNumber;
	
	@FindBy(css="a[class='button evo-button button-action round text-center ng-binding']")	
	public  WebElement exportErrorFile;
	
	@FindBy(id="fileuploadRemediated")	
	public  WebElement fileUploadRemediated;
	
	
	
	
	
	
	
			
			
	
	
	
	
	
	public String filePath;
	
	


	public void navigateToEmployeesPage(WebDriver driver) {
	
		System.out.println("Navigating to emplolyee page..");
		waitForElementToDisplay(employeesDropDown);
		hoverAndClickOnElement(driver, employeesDropDown, viewEmployees);
		waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));
		
	}
	
	
	public void selectEmployee(WebDriver driver) throws InterruptedException {		
		
		
		clickElement(firstEmployee);
		waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));
		
	}
	
	public void addNewEmployee(WebDriver driver) throws InterruptedException {
		
		clickElement(addNewEmployee);
		waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));
	}
	

	public void navigateToFileUpload(WebDriver driver) throws InterruptedException {
		clickElement(uploadPage);
	}
	
	public void exportPopulation(WebDriver driver,FileOperations file,String cmpnyID) throws InterruptedException {
		clickElement(exportPopulation);
		waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));	
		
		file.deleteFile(cmpnyID);
		String userHome = System.getProperty("user.home");
		filePath = userHome+"\\Downloads\\"+cmpnyID+"_EmployeeData.xlsx";	
		SikuliActions s = new SikuliActions();
		Thread.sleep(15000);	
		try {
			//Thread.sleep(30000);
			s.click("Save.PNG");
			Thread.sleep(1000);
		} catch (Exception e) {
			System.out.println("ErrorThrown");
		}
		
	}
	
	public void fileUpload(WebDriver driver,String filePath) throws InterruptedException {
		waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));	
		choosedFileButton.sendKeys(filePath);
		clickElement(uploadButton);
		/*waitForElementToDisplay(By.xpath("//span[contains(text(),'Upload & Validation Success!')]"));
		System.out.println(validationSuccess.getText());*/
		
		
	}
	
	public void fixError(WebDriver driver,String type,FileOperations file) throws InterruptedException, IOException {
		if(type.equalsIgnoreCase("online")) {
			waitForElementToDisplay(By.xpath("//span[contains(text(),'Your file has been uploaded, but it contained errors.')]"));
			clickElement(errorTab);
			
			/*clickElement(editEmployee1);
			clickElement(reSubmitButton);
			System.out.println("loaded1");
			waitForElementToDisplay(By.cssSelector("div[class='processing-request']"));
			System.out.println("loaded2");
			waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));	
			System.out.println("loaded3");
			waitForElementToDisplay(By.xpath("(//a[@data-tooltip='Fix Error'])[2]"));
			clickElement(editEmployee2);
			clickElement(reSubmitButton);
			waitForElementToDisplay(By.cssSelector("div[class='processing-request']"));
			waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));				
			waitForElementToDisplay(By.xpath("(//a[@data-tooltip='Fix Error'])[3]"));
			clickElement(editEmployee3);
			clickElement(reSubmitButton);
			
			waitForElementToDisplay(By.cssSelector("div[class='processing-request']"));*/
			
			waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));
			waitForElementToDisplay(By.xpath("(//a[@data-tooltip='Fix Error'])[1]"));
			clickElement(driver.findElement(By.xpath("(//a[@data-tooltip='Fix Error'])[1]")));
			setInput(newValue, "1234567890");
			clickElement(save);
			waitForElementToDisplay(By.cssSelector("div[class='processing-request']"));		
			waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));	
			waitForElementToDisplay(By.xpath("(//a[@data-tooltip='Fix Error'])[1]"));
			clickElement(driver.findElement(By.xpath("(//a[@data-tooltip='Fix Error'])[1]")));
			setInput(newValue, "test@mercer.com");
			clickElement(save);
			waitForElementToDisplay(By.cssSelector("div[class='processing-request']"));		
			waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));	
			waitForElementToDisplay(By.xpath("(//a[@data-tooltip='Fix Error'])[1]"));
			clickElement(driver.findElement(By.xpath("(//a[@data-tooltip='Fix Error'])[1]")));
			setInput(newValue, "USA");
			clickElement(save);
			waitForElementToDisplay(By.cssSelector("div[class='processing-request']"));		
			waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));
			System.out.println(validationSuccess.getText());
		}else {
			
			waitForElementToDisplay(By.xpath("//span[contains(text(),'Your file has been uploaded, but it contained errors.')]"));
			clickElement(errorTab);
			
		/*	clickElement(editEmployee1);
			clickElement(reSubmitButton);
			System.out.println("loaded1");
			waitForElementToDisplay(By.cssSelector("div[class='processing-request']"));
			System.out.println("loaded2");
			waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));	
			System.out.println("loaded3");
			waitForElementToDisplay(By.xpath("(//a[@data-tooltip='Fix Error'])[2]"));
			clickElement(editEmployee2);
			clickElement(reSubmitButton);
			waitForElementToDisplay(By.cssSelector("div[class='processing-request']"));
			waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));	
			System.out.println();
			waitForElementToDisplay(By.xpath("(//a[@data-tooltip='Fix Error'])[3]"));
			clickElement(editEmployee3);
			clickElement(reSubmitButton);
			waitForElementToDisplay(By.cssSelector("div[class='processing-request']"));*/
			
			waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));
			System.out.println("Clicking Export File button");
			
			filePath=  System.getProperty("user.home")+"\\Downloads\\EmployeeData.xlsx";
			Files.deleteIfExists(Paths.get(filePath)); 
			System.out.println("FileDeleted: "+filePath);	
			
			clickElement(exportErrorFile);
			SikuliActions s = new SikuliActions();
			Thread.sleep(10000);	
			try {
				//Thread.sleep(30000);
				s.click("Save.PNG");
				System.out.println("FileDownloaded..");
				Thread.sleep(1000);
			} catch (Exception e) {
				System.out.println("ErrorThrown");
			}
			
			file.fixErrorFile();	
			
			fileUploadRemediated.sendKeys(file.filePath);
			waitForElementToDisplay(validationSuccess);
			System.out.println(validationSuccess.getText());
			
			
			
			
		}
		
	}


}