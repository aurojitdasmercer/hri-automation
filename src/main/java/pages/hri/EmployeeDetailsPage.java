package pages.hri;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.setInput;
import static driverfactory.Driver.scrollToElement;
import static driverfactory.Driver.waitForElementToDisappear;
import static driverfactory.Driver.waitForElementToDisplay;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import static driverfactory.Driver.hoverAndClickOnElement;

public class EmployeeDetailsPage {
	WebDriver driver;
	
	public EmployeeDetailsPage(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(css="a[ui-sref='hr.employeeProfile.editProfileInfo']")	
	 public WebElement editIcon;	
	
	@FindBy(name="effectiveDate")	
	 public WebElement effectiveDateOfChange;	
	
	@FindBy(xpath="(//button[@class='ui-datepicker-trigger'])[1]")	
	 public WebElement effectiveDateOfChangeDatePcikerClose;
	
	@FindBy(name="firstName")	
	 public WebElement firstName;
	
	@FindBy(name="lastName")	
	 public WebElement lastName;
	
	@FindBy(name="addressLine1")	
	 public WebElement addressLine1;
	
	@FindBy(name="addressLine2")	
	 public WebElement addressLine2;
	
	@FindBy(name="hrCity")	
	 public WebElement city;
	
	@FindBy(name="stateOption")	
	 public WebElement state;
	
	@FindBy(name="hrZip")	
	 public WebElement zip;
	
	@FindBy(name="genderOption")	
	 public WebElement gender;
	
	@FindBy(name="dateOfBirth")	
	 public WebElement dateOfBirth;
	
	@FindBy(css="input[ng-focus='editSsn()']")	
	 public WebElement ssnElement;
	
	@FindBy(xpath="(//button[@class='ui-datepicker-trigger'])[2]")	
	 public WebElement dateOfBirthPcikerClose;
	
	@FindBy(name="phone")	
	 public WebElement homePhone;
	
	@FindBy(name="homeEmail")	
	 public WebElement homeEmail;
	
	@FindBy(name="companyEmail")	
	 public WebElement companyEmail;
	
	@FindBy(name="dateHire")	
	 public WebElement hireDate;
	
	@FindBy(name="jobTitle")	
	 public WebElement jobTitle;
	
	@FindBy(name="workStateOption")	
	 public WebElement workState;
	
	@FindBy(name="statusOption")	
	 public WebElement status;
	
	@FindBy(name="divisionOption")	
	 public WebElement division;
	
	@FindBy(name="locationOption")	
	 public WebElement location;
	
	@FindBy(name="countryOption")	
	 public WebElement country;
	
	@FindBy(name="executiveStatusOption")	
	 public WebElement executiveOption;
	
	@FindBy(name="currentSalary")	
	 public WebElement currentSalary;
	
	@FindBy(name="benefitSalary")	
	 public WebElement benifitSalary;
	
	@FindBy(name="payrollFrequencyOption")	
	 public WebElement payCalendar;
	
	@FindBy(name="salaryOption")	
	 public WebElement pay;
	
	@FindBy(css="button[ng-click='saveBtnClick()']")	
	 public WebElement saveButtonProfile;
	
	@FindBy(css="a[ui-sref='hr.employeeProfile.access']")	
	 public WebElement accessButton;
	
	@FindBy(css="label[for='roleHr']")	
	 public WebElement masterHrAdmin;
	
	
	@FindBy(css="button[ng-click='saveBtnClick()']")	
	 public WebElement accessSave;
	
	@FindBy(name="eeStatusOption")	
	 public WebElement employementStatus;
	
	@FindBy(name="officeTelephone")	
	 public WebElement officeTelephone;
	
	
	
	
	String currentDate;
	Select select;
	
	public String firstname = "TestName001";
	public String lastname = "Lname";
	public String email = "Ab2130201@home.com";
	public String wemail = "Ab1@work.com";
	public String randSSN;
	public String urlSSN;
	
	
	void calculateDates() {
		System.out.println("Calculating Dates...");
		Date currentDate = new Date();
		DateFormat dateformat = new SimpleDateFormat("MM/dd/yyyy");
		this.currentDate = dateformat.format(currentDate);
		System.out.println(this.currentDate);
	}
	
	public int generateSSN() {
		int max = 999999999;
		int min = 960823766;		
		
		return  (int)(Math.random()*((max-min)+1))+min;
		
		
	}


	public void edit(WebDriver driver) throws InterruptedException {
	
		clickElement(editIcon);
		waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));
		
	}
	
	public void editProfileDetalis(WebDriver driver) throws InterruptedException {
		
		calculateDates();
		
		System.out.println("Editing Profile Details...");	
	//	scrollToElement(driver, effectiveDateOfChange);
		
		setInput(effectiveDateOfChange, currentDate);		
		clickElement(effectiveDateOfChangeDatePcikerClose);
		setInput(dateOfBirth, "01/22/1980");
		clickElement(dateOfBirthPcikerClose);
		
		setInput(firstName, firstname);
		setInput(lastName, lastname);
		setInput(addressLine1, "Address Line1");
		setInput(addressLine2, "Address Line 2");
		setInput(homePhone, "(919) 833-1485");
		setInput(city, "Louisville");
		select = new Select(state);
		select.selectByVisibleText("Indiana");
		setInput(zip, "12345");
		select = new Select(gender);
		select.selectByIndex(2);
		
		
		//setInput(ssn, "213-02-0001");
		scrollToElement(driver, homePhone);
		
		setInput(homeEmail, email);
		setInput(companyEmail, wemail);
		setInput(hireDate, "06/01/2010");
		setInput(jobTitle, "HR");
		select = new Select(workState);
		select.selectByIndex(6);
		select = new Select(status);
		select.selectByIndex(2);
		select = new Select(division);
		select.selectByIndex(2);
		select = new Select(location);
		select.selectByIndex(2);
		select = new Select(executiveOption);
		select.selectByIndex(2);
		setInput(currentSalary, "80000");
		setInput(benifitSalary, "95000");
		scrollToElement(driver, payCalendar);
		select = new Select(payCalendar);
		select.selectByIndex(1);
		select = new Select(pay);
		select.selectByIndex(1);
		System.out.println("Selected Pay");
		String temp  =driver.getCurrentUrl();
		urlSSN = temp.substring(87, 96);
		clickElement(saveButtonProfile);
		waitForElementToDisplay(By.cssSelector("div[class='processing-request']"));
		waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));	
		clickElement(accessButton);
		waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));	
		/*if(driver.findElement(By.cssSelector("label[for='roleHr']"))!=null) {
			clickElement(masterHrAdmin);	
			clickElement(accessSave);
			waitForElementToDisplay(By.cssSelector("div[class='processing-request']"));
			waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));	
		}else {
			System.out.println("User is Employee Only...");
		}
			*/
	}
	
	
public void addNewProfileDetails(WebDriver driver) throws InterruptedException {
		
		
		setInput(firstName, firstname);
		setInput(lastName, lastname);
		setInput(addressLine1, "Address Line1");
		setInput(addressLine2, "Address Line 2");
		setInput(dateOfBirth, "01/22/1980");
		randSSN  = Integer.toString(generateSSN());
		System.out.println(randSSN);
		ssnElement.sendKeys(randSSN);
		
		setInput(city, "Louisville");
		select = new Select(state);
		select.selectByVisibleText("Indiana");
		setInput(zip, "46001");
		select = new Select(gender);
		select.selectByIndex(2);
		
		//clickElement(dateOfBirthPcikerClose);
		
	//	setInput(ssn, "213-02-0001");
		/*randSSN  = Integer.toString(generateSSN());
		System.out.println(randSSN);
		ssnElement.sendKeys(randSSN);*/
		
		
		scrollToElement(driver, ssnElement);
		setInput(homePhone, "(919) 833-1485");
		//setInput(officeTelephone, "(919) 833-1486");		
		setInput(homeEmail, email);
		setInput(companyEmail, wemail);
		setInput(hireDate, "12/17/2019");
		setInput(jobTitle, "HR");
		select = new Select(workState);
		select.selectByVisibleText("Hawaii");
		select = new Select(country);
		select.selectByVisibleText("United States");	
		//officeTelephone.sendKeys("(919) 833-1486");
		select = new Select(employementStatus);
		select.selectByVisibleText("Active ");		
		select = new Select(status);
		select.selectByIndex(2);
		select = new Select(division);
		select.selectByIndex(2);
		select = new Select(location);
		select.selectByIndex(2);
		select = new Select(executiveOption);
		select.selectByIndex(2);
		setInput(currentSalary, "80000");
		setInput(benifitSalary, "95000");
		select = new Select(payCalendar);
		select.selectByIndex(1);
		select = new Select(pay);
		select.selectByIndex(1);
		clickElement(saveButtonProfile);
		waitForElementToDisplay(By.cssSelector("div[class='processing-request']"));
		waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));
		
	}
	



}