package pages.hri;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.sikuli.script.FindFailed;

import regression.hri.tests.FileOperations;

import static driverfactory.Driver.waitForElementToDisplay;
import utilities.SikuliActions;import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static driverfactory.Driver.scrollToElement;
import static driverfactory.Driver.setInput;
import static driverfactory.Driver.waitForElementToDisappear;
import static driverfactory.Driver.clickElement;
public class ConsultantPortal {
	WebDriver driver;
	
	public ConsultantPortal(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	Date openEnrollmentStartDate_date;
	Date openEnrollmentEndDate_date;
	Date planYearEffectiveDate_date;
	Date payrollDate_date;
	DateFormat  dateformat;
	public String cmpnyId;
	public String layout;
	public String conversionFlag;
	
	@FindBy(css="a[data-e2e-name='add-new-client']")	
	public  WebElement addNewClient;
	
	@FindBy(css="button[ng-click='goToButton(clientProfile)']")	
	public  WebElement startNow;	
	
	@FindBy(css="select[ng-show='clientId.length === 0']")	
	public  WebElement clientType;	
	
	@FindBy(css="input[placeholder='Client Name']")	
	public  WebElement clientName;	
	
	@FindBy(css="select[ng-model='profileInfo.industry']")	
	public  WebElement clientIndustry;	
	
	@FindBy(css="select[ng-model='profileInfo.employerEntity']")	
	public  WebElement employerEntity;	
	
	@FindBy(css="select[ng-model='selectedBrokerage']")	
	public  WebElement brokerageOfRecord;
	
	@FindBy(xpath="//div[contains(text(),\"Choose One...\")]")	
	public  WebElement primaryConsultant;
	
	@FindBy(css="div[data-e2e-name='Rahul Bhosale']")	
	public  WebElement rahulBhosale;
	
	@FindBy(css="input[placeholder='Client address line 1']")	
	public  WebElement addressLine1;
	
	@FindBy(css="input[placeholder='Client address line 2']")	
	public  WebElement addressLine2;
	
	@FindBy(css="input[placeholder='Client city']")	
	public  WebElement clientCity;
	
	@FindBy(css="select[name='state']")	
	public  WebElement clientState;
	
	@FindBy(css="input[ng-model='profileInfo.clientLocationInfo.zip']")	
	public  WebElement clicentZip;
	
	@FindBy(css="input[placeholder='Contact Name']")	
	public  WebElement contactName;
	
	@FindBy(css="input[placeholder='Contact email']")	
	public  WebElement contactEmail;
	
	@FindBy(css="input[ng-model='profileInfo.hrContactInfo.phone']")	
	public  WebElement contactPhone;
	
	@FindBy(css="button[ng-click='saveBtnClick()']")	
	public  WebElement clientProfileContinuePage1;
	
	@FindBy(css="label[for='size1']")	
	public  WebElement noOfEligibleEmployees;
	
	@FindBy(css="input[name='openEnrollmentBeginDate']")	
	public  WebElement openEnrollmentStartDate;
	
	@FindBy(css="input[name='openEnrollmentEndDate']")	
	public  WebElement openEnrollmentEndDate;	
	
	@FindBy(css="input[name='planBeginEffectiveDate']")	
	public  WebElement planYearEffectiveDate;
	
	@FindBy(css="select[name='situsStateChoice']")	
	public  WebElement situsState;
	
	@FindBy(css="input[name='federalTaxId']")	
	public  WebElement federalTaxId;
	
	@FindBy(css="input[name='clientOneCode']")	
	public  WebElement clientOneCode;
	
	@FindBy(css="select[ng-model='client.layoutIndicator']")	
	public  WebElement layoutIndicator;
	
	@FindBy(css="label[for='CONVERSION-ON']")	
	public  WebElement upgradeClientYes;
	
	@FindBy(css="label[for='CONVERSION-OFF']")	
	public  WebElement upgradeClientNo;
	
	@FindBy(css="label[for='customFileSupport-ON']")	
	public  WebElement customFileSupportYes;
	
	@FindBy(css="button[ng-click='saveBtn2Click()']")	
	public  WebElement clientProfileContinuePage2;
	
	@FindBy(css="label[for='MEDICAL-ON']")	
	public  WebElement medicalYes;
	
	@FindBy(css="label[for='BLIFE-ON']")	
	public  WebElement basicEmployeeLifeYes;
	
	@FindBy(css="label[for='STD-ON']")	
	public  WebElement shortTermDisabilityYes;
	
	@FindBy(css="a[data-e2e-name='Save & Continue']")	
	public  WebElement planDescisionContinuePage1;
	
	@FindBy(css="button[data-e2e-name='Save & Continue']")	
	public  WebElement planDescisionContinuePage2;
	
	@FindBy(css="a[data-e2e-id='MEDICAL']")	
	public  WebElement medicalGetStarted;
	
	@FindBy(css="label[for='AETNA_0']")	
	public  WebElement aetna;
	
	@FindBy(css="label[for='CDHH003E01']")	
	public  WebElement deductableHsa;
	
	@FindBy(css="label[for='OACH003E01']")	
	public  WebElement indemnityDeductableHsa;
	
	@FindBy(css="span[class='icon-toggle-arrow']")	
	public  WebElement expandSelectYourPlans;
	
	@FindBy(css="button[data-e2e-name='Save & Continue']")	
	public  WebElement continueSelectBenifits;
	
	@FindBy(css="select[ng-model='carrierIdHolder.carrierId']")	
	public  WebElement careerSelect;
	
	@FindBy(css="button[ng-click='saveBtnClick()']")	
	public  WebElement continueCareerSelect;
	
	@FindBy(css="button[ng-click='saveBtnClick()']")	
	public  WebElement continueAccounts;
	
	@FindBy(xpath="//select[@name='carrierId']")	
	public  WebElement lifeSelect;
	
	@FindBy(css="label[for='25KN037E01']")	
	public  WebElement lifePlan1;
	
	@FindBy(css="label[for='50KN037E01']")	
	public  WebElement lifePlan2;
	
	@FindBy(css="button[ng-click='save()']")	
	public  WebElement lifeContinue;
	
	@FindBy(xpath="//select[@name='carrierId']")	
	public  WebElement disabilitySelect;
	
	@FindBy(css="label[for='ERPL037E01']")	
	public  WebElement disabilityPlan1;
	
	@FindBy(css="label[for='ERPL037E02']")	
	public  WebElement disabilityPlan2;
	
	@FindBy(css="button[ng-click='save()']")	
	public  WebElement disabilityContinue;
	
	@FindBy(id="electionDownload")	
	public  WebElement electionMappingDownload;
	
	@FindBy(css="input[id='electionMappingUploadFile']")	
	public  WebElement electionMappingUpload;
	
	@FindBy(css="div[class='button-lead ng-scope']")	
	public  WebElement electionMappingUploadButton;
	
	@FindBy(css="button[class='button btn-next radius evo-button ng-binding']")	
	public  WebElement electionMappingContinue;
	
	@FindBy(css="select[name='contributionType']")	
	public  WebElement selectContribution;
	
	@FindBy(css="button[ng-click='saveBtnClick()']")	
	public  WebElement contributionContinue;
	
	@FindBy(xpath="(//input[@type='text'])[1]")	
	public  WebElement hsaContributionClientOnly;
	
	@FindBy(xpath="(//input[@type='text'])[2]")	
	public  WebElement hsaContributionEmployeeSpouse;
	
	@FindBy(xpath="(//input[@type='text'])[3]")	
	public  WebElement hsaContributionEmployeeChild;

	@FindBy(xpath="(//input[@type='text'])[4]")	
	public  WebElement hsaContributionEmployeefamily;
	
	@FindBy(css="button[ng-click='saveBtnClick()']")	
	public  WebElement hsaContributionContinue;
	
	@FindBy(css="button[ng-click='saveBtnClick()']")	
	public  WebElement questionareContinue;
	
	@FindBy(css="a[ng-click='nextButton()']")	
	public  WebElement continueToOnscreenEntry;
	
	@FindBy(xpath="(//input[@type='text'])")	
	public  List<WebElement> medicalWorksheetEntry;
	
	@FindBy(css="button[ng-click='saveChanges()']")	
	public  WebElement medicalWorksheetSave;
	
	@FindBy(xpath="(//div[@class='large-3 medium-3 small-12 columns ng-scope'])[2]")	
	public  WebElement lifeWorksheetNav;
	
	@FindBy(xpath="(//div[@class='large-3 medium-3 small-12 columns ng-scope'])[3]")	
	public  WebElement disabilityWorksheetNav;
	
	@FindBy(css="input[type='text']")	
	public  List<WebElement> lifeWorksheetEntry;
	

	@FindBy(id="optionAmount.id")	
	public WebElement lifeWorksheetOptionAmt;
	
	@FindBy(xpath="(//input[@type='text'])[1]")	
	public WebElement lifeWorksheetCover1;
	
	@FindBy(xpath="(//input[@type='text'])[2]")	
	public WebElement lifeWorksheetCover2;
	
	@FindBy(xpath="(//input[@type='text'])[3]")	
	public WebElement lifeWorksheetCover3;
	
	@FindBy(xpath="(//input[@type='text'])[4]")	
	public WebElement lifeWorksheetCover4;
	
	@FindBy(css="button[ng-click='saveChanges()']")	
	public  WebElement lifeWorksheetSave;
	
	@FindBy(id="optionAmount.id")	
	public WebElement disabilityWorksheetOptionAmt;
	
	@FindBy(xpath="(//input[@type='text'])[2]")	
	public WebElement disabilityWorksheetCover2;
	
	@FindBy(xpath="(//input[@type='text'])[3]")	
	public WebElement disabilityWorksheetCover3;
	
	@FindBy(xpath="(//input[@type='text'])[4]")	
	public WebElement disabilityWorksheetCover4;
	
	@FindBy(css="button[ng-click='saveAndContinue()']")	
	public WebElement ratesSaveAndContinue;
	
	@FindBy(css="button[ng-click='saveBtnClick()']")	
	public WebElement openEnrolmenSaveAndContinue;
	
	@FindBy(css="input[data-e2e-name='waiting-period']")	
	public WebElement employeeWaitingPeriod;
	
	@FindBy(css="button[ng-click='saveBtnClick()']")	
	public WebElement newHiresSaveAndContinue;
	
	@FindBy(css="button[ng-click='completeStep()']")	
	public WebElement reHiresSaveAndContinue;
	
	@FindBy(css="button[ng-click='saveBtnClick()']")	
	public WebElement terminationSaveAndContinue;
	
	@FindBy(css="button[ng-click='saveBtnClick()']")	
	public WebElement lifeEventsSaveAndContinue;
	

	@FindBy(css="select[ng-model='selectedTemplateOption']")	
	public WebElement selectPayroll;
	
	@FindBy(css="input[name='payrollFileDate'] ")	
	public WebElement payrollFileDate;
	
	@FindBy(css="button[ng-click='saveBtnClick()']")	
	public WebElement payrollSaveandContinue;
	
	@FindBy(css="label[for='monthly']")	
	public WebElement payrollFrequencyMonthly;
	
	@FindBy(css="a[ng-click='saveBtnClick()']")	
	public WebElement payrollFrequencySaveandContinue;
	
	@FindBy(css="button[ng-click='confirm()']")	
	public WebElement payrollCalenderConfirm;
	
	@FindBy(css="button[ng-click='saveBtnClick()']")	
	public WebElement shortTermDisabilitySaveandContinue;
	
	@FindBy(css="button[ng-click='saveBtnClick()']")	
	public WebElement roundingRulesSaveandContinue;
	
	@FindBy(css="select[ng-model='selectedAgeReductionRule']")	
	public WebElement ageReduction;
	
	@FindBy(xpath="(//input[@type='text'])[1]")	
	public WebElement ageReductionScheduleAge;
	
	@FindBy(xpath="(//input[@type='text'])[2]")	
	public WebElement ageReductionSchedulePercent;
	
	@FindBy(css="select[ng-model='selectedRoundingRule']")	
	public WebElement roundingRule;
	
	@FindBy(xpath="(//input[@type='text'])[3]")	
	public WebElement roundingIncrement;
	
	@FindBy(css="button[ng-click='saveBtnClick()']")	
	public WebElement ageReductionSaveandContinue;
	
	@FindBy(css="button[ng-click='saveBtnClick()']")	
	public WebElement clientLogoSaveandContinue;
	
	@FindBy(css="button[ng-click='saveButtonClick()']")	
	public WebElement messageSaveandContinue;
	
	@FindBy(css="a[ng-click='continueBtnClick()']")	
	public WebElement addDocumentSaveandContinue;
	
	@FindBy(css="input[name='monthlyDate']")	
	public WebElement payrollDateMonthly;
	
	@FindBy(xpath="(//button[@class='ui-datepicker-trigger'])[4]")	
	public WebElement payrollDateMonthlyButton;
	
	@FindBy(css="select[ng-model='carrierIdHolder.carrierId']")	
	public WebElement cobraBenifits;
	
	@FindBy(css="input[name='answer3']")	
	public WebElement cobraMembersEnrolled;
	
	@FindBy(css="input[name='answer4']")	
	public WebElement cobraMembersPending;
	
	@FindBy(css="button[ng-click='saveAndContinueBtnClick()']")	
	public WebElement cobraSaveandContinue;
	
	@FindBy(css="select[name='callCenterOption']")	
	public WebElement callCentreSelect;
	
	@FindBy(css="button[ng-click='saveAndContinueBtnClick()']")	
	public WebElement callCentreSaveandContinue;
	
	@FindBy(css="button[ng-click='saveAndContinueBtnClick()']")	
	public WebElement coverageDetailsSaveandContinue;
	
	@FindBy(css="button[ng-click='nextStepSref()']")	
	public WebElement demographicUploadNext;
	
	@FindBy(css="label[for='demographicUpload']")	
	public WebElement demographicUploadChooseFileButton;
	
	@FindBy(css="input[id='demographicUpload']")	
	public WebElement demographicUploadFile;
	
	@FindBy(css="div[class='button-lead-icon-box']")	
	public WebElement demographicUploadFileButton;
	
	@FindBy(css="a[ng-click='processOEForConversionClients()']")	
	public WebElement processOeButton;
	
	@FindBy(css="p[class='lead text-center ng-binding']")	
	public WebElement uploadSuccessText;
	
	@FindBy(xpath="//span[contains(text(),'Upload & Validation Success!')]")	
	public WebElement uploadSuccessText2;
	
	@FindBy(css="button[class='button btn-next radius evo-button ng-binding']")	
	public WebElement demographicUploadNext2;
	
	@FindBy(css="button[ng-click='saveBtnClick()']")	
	public WebElement automatedLoadSaveandContinue;
	
	
	
	@FindBy(xpath="(//input[@name='elementValue'])[1]")	
	public WebElement clientCodenameAetna;
	
	@FindBy(xpath="(//input[@name='elementValue'])[2]")	
	public WebElement policyNumberAetna;
	
	@FindBy(xpath="(//input[@name='elementValue'])[3]")	
	public WebElement controlNumberAetna;
	
	@FindBy(css="label[for='MEDICAL--AETNA_0--3yes']")	
	public WebElement suffixCodeYes;
	
	@FindBy(css="label[for='MEDICAL--AETNA_0--4yes']")	
	public WebElement planCodeYes;
	
	@FindBy(css="label[for='MEDICAL--AETNA_0--6yes")	
	public WebElement accountCodeYes;	
	

	@FindBy(css="label[for='MEDICAL--AETNA_0--8yes']")	
	public WebElement elrYes;	
	
	
	@FindBy(css="input[ng-model='plan.value']")	
	public List<WebElement> planValues;
	
	@FindBy(css="a[ng-click='save()']")	
	public WebElement accountStructureMedicalSave;
	
	@FindBy(css="label[for='HSA--FIDELTY--2yes']")	
	public WebElement hsaYes;
	
	@FindBy(css="label[for='HSA--FIDELTY--3yes']")	
	public WebElement hphpYes;
	
	@FindBy(css="input[type='text']")	
	public WebElement spendingAccountPlanValue;
	
	@FindBy(css="a[ng-click='save()']")	
	public WebElement accountStructureHsaSave;
	
	@FindBy(css="input[type='text']")	
	public List<WebElement> accountStructureLifeText;
	
	@FindBy(css="label[for='BLIFE--AETNA_1--3yes']")	
	public WebElement clientSuffixYes;
	
	@FindBy(css="label[for='BLIFE--AETNA_1--4yes']")	
	public WebElement clientAccountYes;
	
	@FindBy(css="a[ng-click='save()']")	
	public WebElement accountStructureLifeSave;
	
	@FindBy(css="input[type='text']")	
	public List<WebElement> accountStructureDisabilityText;
	
	@FindBy(css="label[for='STD--AETNA_1--3yes']")	
	public WebElement disabilityClientSuffixYes;
	
	@FindBy(css="label[for='STD--AETNA_1--4yes']")	
	public WebElement disabilityClientAccountYes;
	
	@FindBy(css="a[ng-click='save()']")	
	public WebElement accountStructureDisabilitySave;
	
	@FindBy(css="button[ng-click='nextClick()']")	
	public WebElement validationNext;
	
	@FindBy(css="label[for='disclamer']")	
	public WebElement finalEmployeeDataAgree;
	
	@FindBy(css="button[ng-click='clickNext()']")	
	public WebElement finalEmployeeDataNext;
	
	@FindBy(css="label[for='disclamer']")	
	public WebElement termsAndConditioinsAgree;
	
	@FindBy(css="button[ng-click='nextStep()']")	
	public WebElement termsAndConditioinsNext;
	
	@FindBy(css="a[ng-click='confirm()']")	
	public WebElement termsAndConditioinsIconfirm;
	
	@FindBy(css="a[ui-sref='dashboard']")	
	public WebElement returnToDashBoard;
	
	@FindBy(css="label[for='base-STD-AETNA_1-ERPL037E01']")	
	public WebElement baseSalary;
	
	@FindBy(css="label[for='benefits-STD-AETNA_1-ERPL037E01']")	
	public WebElement benifitsSalary;
	
	@FindBy(css="button[ng-click='saveBtnClick()']")	
	public WebElement frozendataSavendContinue;
	
	@FindBy(css="a[ng-click='openTestingPortal()']")	
	public WebElement accessTestingPortal;
	
	@FindBy(css="a[ng-click='openPersonalization()']")	
	public WebElement customiseClientsDataValue;	
	
	@FindBy(xpath="(//a[@class='ng-binding'])[12]")	
	public WebElement derivedFactorsTab;
	
	@FindBy(css="a[ng-disabled='factor && factor.isEditing']")	
	public WebElement addNewDerivedFactors;
	
	@FindBy(css="label[for='mapping']")	
	public WebElement mapping;	
	
	@FindBy(css="select[ng-model='derivation.expression']")	
	public WebElement selectMapping;	
	
	@FindBy(css="a[ng-click='save()']")	
	public WebElement saveMapping;
	
	@FindBy(css="button[ng-click='close()']")	
	public WebElement closeMapping;
	
	@FindBy(xpath="(//a[@class='ng-binding'])[11]")	
	public WebElement customFactorsTab;
	
	@FindBy(css="a[ng-click='addValue()']")	
	public WebElement addValue;
	
	@FindBy(name="value")	
	public WebElement value;
	
	@FindBy(name="nickname")	
	public WebElement nickName;
	
	@FindBy(css="a[ng-click='save()']")	
	public WebElement saveValue;
	
	@FindBy(xpath="//li[contains(text(),'Custom Factor 2')]")	
	public WebElement customFactor2;
	
	@FindBy(css="li[data-e2e-id='32']")	
	public WebElement customFactor3;
	
	@FindBy(css="li[data-e2e-id='33']")	
	public WebElement customFactor4;
	
	@FindBy(css="li[data-e2e-id='34']")	
	public WebElement customFactor5;
	
	
	
	
	Select select;
	
	public void calculateDates() {
		Date currentDate = new Date();
		dateformat = new SimpleDateFormat("MM/dd/yyyy");
		DateFormat CurrentMonth =  new SimpleDateFormat("MM");
		DateFormat Currentyear =  new SimpleDateFormat("yyyy");
		int crntMnth = Integer.parseInt(CurrentMonth.format(currentDate));
		int crntYear = Integer.parseInt(Currentyear.format(currentDate)); 
		System.out.println("CurrentDate :"+dateformat.format(currentDate));
		Calendar c = Calendar.getInstance();
        c.setTime(currentDate);
        c.add(Calendar.DATE, 7);
        openEnrollmentStartDate_date = c.getTime();
        System.out.println("Open Enrollment Start date: "+dateformat.format(openEnrollmentStartDate_date));
        c.setTime(currentDate);
        c.add(Calendar.DATE, 14);
        openEnrollmentEndDate_date = c.getTime();
        c.setTime(openEnrollmentEndDate_date);
        c.add(Calendar.DATE, 8);
        payrollDate_date = c.getTime();
        System.out.println("OpenEnrollMEntEndDate:  "+dateformat.format(openEnrollmentEndDate_date));   
        System.out.println("PayrollDate: "+dateformat.format(payrollDate_date)); 
            
        c.set(crntYear,crntMnth,1);
        c.add(Calendar.MONTH, 1);
        //c.set(Calendar.MONTH, 1);
        planYearEffectiveDate_date = c.getTime();
        System.out.println("PlanYear Date: "+dateformat.format(planYearEffectiveDate_date));
	}
	
	public void addNewClient(WebDriver driver) throws InterruptedException {
		System.out.println("AddingNewClient");
		waitForElementToDisplay(addNewClient);
		scrollToElement(driver, addNewClient);
		clickElement(addNewClient);
		clickElement(startNow);		
	}
	
	public void createClientProfile(WebDriver driver, String layout,String conversionFlag ) throws InterruptedException {
		this.layout=  layout;
		this.conversionFlag = conversionFlag;
		System.out.println("CreateClientProfile");
		waitForElementToDisplay(clientType);
		select = new Select(clientType);
		select.selectByVisibleText("Custom");
		setInput(clientName, "TestClient001");
		select = new Select(clientIndustry);
		select.selectByIndex(7);
		select = new Select(employerEntity);
		select.selectByIndex(3);
		select = new Select(brokerageOfRecord);
		select.selectByVisibleText("QA Test 2");
		clickElement(primaryConsultant);
		scrollToElement(driver, rahulBhosale);
		waitForElementToDisplay(rahulBhosale);
		clickElement(rahulBhosale);
		setInput(addressLine1, "Test");
		setInput(addressLine2, "Test");
		setInput(clientCity, "NY");
		setInput(clicentZip, "12345");
		setInput(contactName, "Test");
		setInput(contactEmail, "a.c@m.com");
		setInput(contactPhone, "9608237667");
		select = new Select(clientState);
		select.selectByIndex(3);
		clickElement(clientProfileContinuePage1);	
		
		System.out.println("ClientProfileCreation page 2..");
		waitForElementToDisplay(noOfEligibleEmployees);
		clickElement(noOfEligibleEmployees);
		calculateDates();
		setInput(openEnrollmentStartDate, dateformat.format(openEnrollmentStartDate_date));
		setInput(openEnrollmentEndDate, dateformat.format(openEnrollmentEndDate_date));
		setInput(planYearEffectiveDate, dateformat.format(planYearEffectiveDate_date));
		select = new Select(situsState);
		select.selectByIndex(7);
		setInput(federalTaxId, "123456789");
		setInput(clientOneCode, "ABHOTW");
		select = new Select(layoutIndicator);
		select.selectByVisibleText(layout);	
		if(layout.equalsIgnoreCase("Enhanced")) {
			if(conversionFlag.equalsIgnoreCase("y")) {
				clickElement(upgradeClientYes);	
			}else {
				clickElement(upgradeClientNo);
			}
			
			clickElement(customFileSupportYes);
		}
		
		clickElement(clientProfileContinuePage2);		
	}
	
	public void selectPlanDescisions(WebDriver driver) throws InterruptedException {
		
		waitForElementToDisplay(medicalYes);
		clickElement(medicalYes);
		clickElement(basicEmployeeLifeYes);
		clickElement(shortTermDisabilityYes);
		scrollToElement(driver, planDescisionContinuePage1);
		clickElement(planDescisionContinuePage1);
		waitForElementToDisplay(planDescisionContinuePage2);
		scrollToElement(driver, planDescisionContinuePage2);
		clickElement(planDescisionContinuePage2);
		waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));
		
	}
	
	public void selectBenifits(WebDriver driver,FileOperations file) throws InterruptedException, FindFailed {
		
		waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));
		//clickElement(driver.findElement(By.cssSelector("a[data-e2e-id='MEDICAL']")));
		clickElement(medicalGetStarted);
		System.out.println("WaitingForPageToLoad...");
		waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));
		System.out.println("PageLoaded...");
		clickElement(aetna);
		scrollToElement(driver, expandSelectYourPlans);
		clickElement(expandSelectYourPlans);
		clickElement(deductableHsa);
		clickElement(indemnityDeductableHsa);
		scrollToElement(driver, continueSelectBenifits);
		clickElement(continueSelectBenifits);	
		System.out.println("WaitingForPageToLoadCareerSelect...");
		waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));
		System.out.println("PageLoaded...");
		waitForElementToDisplay(careerSelect);		
		select = new Select(careerSelect);
		select.selectByIndex(3);
		clickElement(continueCareerSelect);
		waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));
		clickElement(continueAccounts);
		System.out.println("WaitingForPageToLoadLIfeSelect...");
		waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));
		System.out.println("PageLoaded...");	
		waitForElementToDisplay(lifeSelect);		
		select = new Select(lifeSelect);
		select.selectByIndex(1);
		scrollToElement(driver, lifePlan1);
		clickElement(lifePlan1);
		clickElement(lifePlan2);		
		clickElement(lifeContinue);			
		waitForElementToDisplay(By.cssSelector("div[class='processing-request']"));
		waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));
		select = new Select(disabilitySelect);
		select.selectByVisibleText("Aetna");
		clickElement(disabilityPlan1);
		clickElement(disabilityPlan2);
		clickElement(disabilityContinue);
		
		if(layout.equalsIgnoreCase("Enhanced")&&conversionFlag.equalsIgnoreCase("y")) {
			waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));
			clickElement(electionMappingDownload);
				
			file.deleteFile();
			SikuliActions s = new SikuliActions();
			Thread.sleep(25000);
			Thread.sleep(25000);
			try {
				//Thread.sleep(30000);
				s.click("Save.PNG");
				Thread.sleep(1000);
			} catch (Exception e) {
				System.out.println("ErrorThrown");
			}
			file.readFile();	
		}
		
		waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));
	}
	
	public void selectContributionStrategy(WebDriver driver) throws InterruptedException {
		waitForElementToDisplay(selectContribution);
		select = new Select(selectContribution);
		select.selectByIndex(1);
		clickElement(contributionContinue);
		waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));
		waitForElementToDisplay(hsaContributionClientOnly);
		setInput(hsaContributionClientOnly, "10000");
		setInput(hsaContributionEmployeeChild, "10000");
		setInput(hsaContributionEmployeeSpouse, "10000");
		setInput(hsaContributionEmployeefamily, "10000");
		clickElement(hsaContributionContinue);
		waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));		
	}
	
	public void selectRates(WebDriver driver) throws InterruptedException{
		waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));	
		clickElement(questionareContinue);
		waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));	
		clickElement(continueToOnscreenEntry);
		waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));	
		
		for(int i=0;i<16;i++) {
			setInput(medicalWorksheetEntry.get(i), "10000");			
		}
		
		clickElement(medicalWorksheetSave);		
		waitForElementToDisplay(By.cssSelector("div[class='processing-request']"));
		waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));
		scrollToElement(driver, lifeWorksheetNav);
		clickElement(lifeWorksheetNav);
		waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));
		waitForElementToDisplay(lifeWorksheetEntry.get(0));			
		setInput(lifeWorksheetOptionAmt, "55");
		//setInput(lifeWorksheetCover1, "57");
		setInput(lifeWorksheetCover2, "95");
		setInput(lifeWorksheetCover3, "70");
		setInput(lifeWorksheetCover4, "60");
		clickElement(lifeWorksheetSave);
		
		waitForElementToDisplay(By.cssSelector("div[class='processing-request']"));
		waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));	
		scrollToElement(driver, disabilityWorksheetNav);
		waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));	
		clickElement(disabilityWorksheetNav);
		waitForElementToDisplay(By.cssSelector("div[class='processing-request']"));
		waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));
		scrollToElement(driver, disabilityWorksheetOptionAmt);
		setInput(disabilityWorksheetOptionAmt, "100");
		setInput(disabilityWorksheetCover2, "95");
		setInput(disabilityWorksheetCover3, "70");
		setInput(disabilityWorksheetCover4, "60");
		clickElement(ratesSaveAndContinue);
		System.out.println("RatesSaveAndContinue");
		//waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));	
	}
	
	public void setAdministration(WebDriver driver) throws InterruptedException{
		System.out.println("Set Administration");
		//waitForElementToDisplay(openEnrolmenSaveAndContinue);
		waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));	
		clickElement(openEnrolmenSaveAndContinue);
		waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));	
		setInput(employeeWaitingPeriod, "7");
		clickElement(newHiresSaveAndContinue);
		waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));
		clickElement(reHiresSaveAndContinue);
		waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));
		clickElement(terminationSaveAndContinue);
		waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));
		clickElement(lifeEventsSaveAndContinue);
		waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));
		waitForElementToDisplay(selectPayroll);
		select = new Select(selectPayroll);
		select.selectByIndex(1);
		
	//	calculateDates();
		
		
		setInput(payrollFileDate, dateformat.format(payrollDate_date));
		clickElement(payrollSaveandContinue);
		waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));
		
		clickElement(payrollFrequencyMonthly);
		scrollToElement(driver, payrollDateMonthly);
		
		setInput(payrollDateMonthly, dateformat.format(planYearEffectiveDate_date));
		clickElement(payrollDateMonthlyButton);
		
		clickElement(payrollFrequencySaveandContinue);
		waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));
		clickElement(payrollCalenderConfirm);
		waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));
		clickElement(shortTermDisabilitySaveandContinue);
		waitForElementToDisplay(By.cssSelector("div[class='processing-request']"));
		waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));		
		scrollToElement(driver, roundingRulesSaveandContinue);
		clickElement(roundingRulesSaveandContinue);
		waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));
		waitForElementToDisplay(ageReduction);
		select = new Select(ageReduction);
		select.selectByIndex(1);
		setInput(ageReductionScheduleAge, "56");
		setInput(ageReductionSchedulePercent, "26");
		select = new Select(roundingRule);
		select.selectByIndex(2);
		setInput(roundingIncrement, "1000");
		clickElement(ageReductionSaveandContinue);
		waitForElementToDisplay(By.cssSelector("div[class='processing-request']"));
		waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));
		clickElement(clientLogoSaveandContinue);
		waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));
		clickElement(messageSaveandContinue);
		waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));
		clickElement(addDocumentSaveandContinue);
		waitForElementToDisplay(By.cssSelector("div[class='processing-request']"));
		waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));
		select = new Select(cobraBenifits);
		select.selectByIndex(2);
		//setInput(cobraMembersEnrolled, "2");
		//setInput(cobraMembersPending, "3");
		clickElement(cobraSaveandContinue);
		waitForElementToDisplay(By.cssSelector("div[class='processing-request']"));
		waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));
		
		select = new Select(callCentreSelect);
		select.selectByIndex(1);
		clickElement(callCentreSaveandContinue);
		
		//-------
		
		waitForElementToDisplay(By.cssSelector("div[class='processing-request']"));
		waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));
		clickElement(coverageDetailsSaveandContinue);
		waitForElementToDisplay(By.cssSelector("div[class='processing-request']"));		
		waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));
		
		
		
		//demographic upload page
		
		clickElement(demographicUploadNext);
		//waitForElementToDisplay(By.cssSelector("div[class='processing-request']"));
		waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));
		//scrollToElement(driver, demographicUploadChooseFileButton);
		//layout = "Standard";
		if(layout.equalsIgnoreCase("standard")) {
			String userdir = System.getProperty("user.dir");
			String employeeFilePath = userdir+"\\src\\main\\resources\\testdata\\EmployeeDataS.xlsx";
			System.out.println("Updating Employee File Path: "+employeeFilePath);
			demographicUploadFile.sendKeys(employeeFilePath);
			clickElement(demographicUploadFileButton);			
			waitForElementToDisplay(uploadSuccessText);		
			System.out.println(uploadSuccessText.getText());
			String url = driver.getCurrentUrl();
			cmpnyId = url.substring(76, 81);
			System.out.println("CompanyId : "+cmpnyId);
		}else if(conversionFlag.equalsIgnoreCase("y")&&layout.equalsIgnoreCase("Enhanced")){
			String userdir = System.getProperty("user.dir");
			String employeeFilePath = userdir+"\\src\\main\\resources\\testdata\\EmployeeData.xlsx";
			System.out.println("Updating Employee File Path: "+employeeFilePath);
			demographicUploadFile.sendKeys(employeeFilePath);
			clickElement(demographicUploadFileButton);
			waitForElementToDisplay(processOeButton);
			clickElement(processOeButton);
			waitForElementToDisplay(uploadSuccessText);		
			System.out.println(uploadSuccessText.getText());
			String url = driver.getCurrentUrl();
			cmpnyId = url.substring(76, 81);
			System.out.println("CompanyId : "+cmpnyId);
		}else {
			String userdir = System.getProperty("user.dir");
			String employeeFilePath = userdir+"\\src\\main\\resources\\testdata\\EmployeeData.xlsx";
			System.out.println("Updating Employee File Path: "+employeeFilePath);
			demographicUploadFile.sendKeys(employeeFilePath);
			clickElement(demographicUploadFileButton);			
			waitForElementToDisplay(uploadSuccessText);		
			System.out.println(uploadSuccessText.getText());
			String url = driver.getCurrentUrl();
			cmpnyId = url.substring(76, 81);
			System.out.println("CompanyId : "+cmpnyId);
		}		
		
	}
	
	public void setAcountStructure(WebDriver driver) throws InterruptedException{
		//driver.get("https://consultant-qaf.mercermarketplace365plus.com/#/pendingClients/wizard/37116/2020/accountStructure/MEDICAL");
		waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));
		setInput(clientCodenameAetna, "AetnaTest01");
		setInput(policyNumberAetna, "1036524");
		setInput(controlNumberAetna, "1036324");
		clickElement(suffixCodeYes);
		clickElement(planCodeYes);
		clickElement(accountCodeYes);
		clickElement(elrYes);
		System.out.println(planValues.size());
		for(int i = 0;i<6;i++) {
			setInput(planValues.get(i), "1567");
		}
		clickElement(accountStructureMedicalSave);
		waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));
		
		clickElement(hsaYes);
		clickElement(hphpYes);
		setInput(spendingAccountPlanValue, "1234");
		clickElement(accountStructureHsaSave);
		waitForElementToDisplay(By.cssSelector("div[class='processing-request']"));
		waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));
		for(int i=0;i<2;i++) {
			setInput(accountStructureLifeText.get(i), "1234567");
		}
		clickElement(clientSuffixYes);
		clickElement(clientAccountYes);
		clickElement(accountStructureLifeSave);
		
		waitForElementToDisplay(By.cssSelector("div[class='processing-request']"));
		waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));
		for(int i=0;i<2;i++) {
			setInput(accountStructureDisabilityText.get(i), "1234567");
		}
		clickElement(disabilityClientSuffixYes);
		clickElement(disabilityClientAccountYes);
		clickElement(accountStructureDisabilitySave);
		waitForElementToDisplay(By.cssSelector("div[class='processing-request']"));
		waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));		
		
		clickElement(validationNext);
		waitForElementToDisplay(By.cssSelector("div[class='processing-request']"));
		waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));	
		clickElement(finalEmployeeDataAgree);
		clickElement(finalEmployeeDataNext);
		waitForElementToDisplay(By.cssSelector("div[class='processing-request']"));
		waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));	
		
		clickElement(termsAndConditioinsAgree);
		clickElement(termsAndConditioinsNext);
		clickElement(termsAndConditioinsIconfirm);
		waitForElementToDisplay(By.cssSelector("div[class='processing-request']"));
		waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));
		clickElement(returnToDashBoard);
		waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));	
	}
	
	public void navigateToAccesstestingPortal(WebDriver driver,String cmpnyID) throws InterruptedException {
		System.out.println("Navigate to access Testing portal...");
		waitForElementToDisplay(addNewClient);
		String cmpnyid= cmpnyID;
		String url = "https://consultant-qaf.mercermarketplace365plus.com/#/pendingClients/wizard/"+cmpnyid+"/2020/validation";
		//driver.get(url);
		
		driver.navigate().to(url);
		System.out.println(url);
		
		waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));	
		clickElement(accessTestingPortal);
		waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));	
	}
	
	
	public void selectDerivedFactors(WebDriver driver) throws InterruptedException {
		
		waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));		
		clickElement(customiseClientsDataValue);
		waitForElementToDisappear(By.cssSelector("img[alt='Loading']"));	
		clickElement(derivedFactorsTab);
		
		String[] values = {"Latest Hire Date","Current Salary","Employment Status","First Name","Gender"};
		
		for(String temp:values) {
			clickElement(addNewDerivedFactors);
			clickElement(mapping);
			select = new Select(selectMapping);
			select.selectByVisibleText(temp);
			clickElement(saveMapping);			
		}
		
		clickElement(closeMapping);
		waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));	
		driver.navigate().refresh();
		waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));
	}
	
public void selectCustomFactors(WebDriver driver) throws InterruptedException {
		
		waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));		
		clickElement(customiseClientsDataValue);
		waitForElementToDisappear(By.cssSelector("img[alt='Loading']"));	
		clickElement(customFactorsTab);
		
		clickElement(addValue);
		setInput(value, "1");
		setInput(nickName, "1");
		clickElement(saveValue);
		
		waitForElementToDisplay(By.cssSelector("div[class='processing-request']"));
		waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));
		
		
		clickElement(customFactor2);		
		clickElement(addValue);
		setInput(value, "C1");
		setInput(nickName, "C1");
		clickElement(saveValue);
		waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));
		
		waitForElementToDisplay(By.cssSelector("div[class='processing-request']"));
		waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));
		
		clickElement(customFactor3);
		clickElement(addValue);
		setInput(value, "Zip");
		setInput(nickName, "Zip=20014");
		clickElement(saveValue);
		waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));
		
		waitForElementToDisplay(By.cssSelector("div[class='processing-request']"));
		waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));
		
		clickElement(customFactor4);
		clickElement(addValue);
		setInput(value, "Fname");
		setInput(nickName, "FirstName");
		clickElement(saveValue);
		waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));
		
		waitForElementToDisplay(By.cssSelector("div[class='processing-request']"));
		waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));
		
		clickElement(customFactor5);
		clickElement(addValue);
		setInput(value, "Lname");
		setInput(nickName, "LastName");
		clickElement(saveValue);
		waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));
		
		waitForElementToDisplay(By.cssSelector("div[class='processing-request']"));
		waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));		
		
		driver.navigate().refresh();
		waitForElementToDisappear(By.cssSelector("div[class='processing-request']"));
		
	
}
	
	
}


