
package pages.hri;

import static driverfactory.Driver.clickElement;
import static driverfactory.Driver.waitForElementToDisplay;
import static utilities.DatabaseUtils.getResultSet;
import static verify.SoftAssertions.assertNotNull;
import static verify.SoftAssertions.verifyEquals;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.aventstack.extentreports.ExtentTest;

import driverfactory.Driver;
import static driverfactory.Driver.*;
import utilities.DatabaseUtils;


public class dbTest{
	
	public ArrayList<String> ScheduleListDB(String ClientOneCode, ExtentTest test) throws ClassNotFoundException, SQLException, ParseException  {
	  	  ArrayList<String> list = new ArrayList();
	  	  
	  	      String companyId=  "37116";
			  Connection mycon= DatabaseUtils.getConnection("oracle.jdbc.driver.OracleDriver", 
					  "jdbc:oracle:thin:@ldap://oid.mercerhrs.com:389/bwqa14,cn=OracleContext,dc=world","Z_MM_MT1_A","MM_MT1_A_zqaFri2019");
			 
			  		System.out.println("Running query1...");
				  ResultSet query1= getResultSet(mycon, "SELECT * FROM COMP_SSN WHERE COMPANY_ID IN ('"+companyId+"')");
				  
				  ResultSetMetaData metaData = query1.getMetaData();
					int columnCount = metaData.getColumnCount();				
					System.out.println(columnCount+" ");
					while(query1.next()) {
					   	String realSsn = (query1.getString(4));
				        list.add(realSsn);
				        System.out.println(realSsn);			 
					    }
					
					System.out.println("Running query2...");
					
					ResultSet query2= getResultSet(mycon, "select * from dep_ssn t where t.ssn in (SELECT SSN FROM COMP_SSN WHERE COMPANY_ID IN ('"+companyId+"'))");
					metaData = query2.getMetaData();
								
					System.out.println(columnCount+" ");
					while(query2.next()) {
					    String depSsn = (query2.getString(3));
					    assertNotNull(depSsn, "Checking ssn", test);
				        System.out.println(depSsn);			 
					    }
					
					System.out.println("Running query3...");
					
					ResultSet query3= getResultSet(mycon, "select t.*, rowid from employee_eff_date t where t.ssn in (SELECT SSN FROM COMP_SSN WHERE COMPANY_ID IN ('"+companyId+"'))");
					metaData = query3.getMetaData();
								
					System.out.println(columnCount+" ");
					while(query3.next()) {
					    String effDate = (query3.getString(3));
					    assertNotNull(effDate, "Checking effDate", test);
				        System.out.println(effDate);			 
					    }
					
					System.out.println("Running query4...");
					
					ResultSet query4= getResultSet(mycon, "select t.*, rowid from dependent_eff_date t where t.ssn in (SELECT SSN FROM COMP_SSN WHERE COMPANY_ID IN ('"+companyId+"'))");
					metaData = query4.getMetaData();
								
					System.out.println(columnCount+" ");
					while(query4.next()) {
					    String dep_effDate = (query4.getString(4));
					    assertNotNull(dep_effDate, "Checking dep_effDate", test);
				        System.out.println(dep_effDate);			 
					    }
					
					System.out.println("Running query5...");
					
					ResultSet query5= getResultSet(mycon, "select t.*, rowid from employee_life_events t where t.ssn in (SELECT SSN FROM COMP_SSN WHERE COMPANY_ID IN ('"+companyId+"'))");
					metaData = query5.getMetaData();
								
					System.out.println(columnCount+" ");
					while(query5.next()) {
					    String lifeEventID = (query5.getString(4));
					    String lifeEventStatus = (query5.getString(11));
					    if(lifeEventID.equalsIgnoreCase("52")) {
					    	verifyEquals(lifeEventStatus, "C", test);					    	
					    }else if (lifeEventID.equalsIgnoreCase("55")) {
					    	verifyEquals(lifeEventStatus, "P", test);					    	
						}
				        System.out.println(lifeEventID+" "+lifeEventStatus);			 
					    }
					
					
					System.out.println("Running query6...");
					
					ResultSet query6= getResultSet(mycon, "select t.*, rowid from emp_ben_elects t where t.ssn in (SELECT SSN FROM COMP_SSN WHERE COMPANY_ID IN ('"+companyId+"'))");
					metaData = query6.getMetaData();
								
					System.out.println(columnCount+" ");
					while(query6.next()) {
					    String lifeEventID = (query6.getString(4));
					    String lifeEventStatus = (query6.getString(11));
					    
				        System.out.println(lifeEventID+" "+lifeEventStatus);			 
					    }
					
					System.out.println("Running query7...");
					
					ResultSet query7= getResultSet(mycon, "select t.*, rowid from emp_ben_elects t where t.ssn in (SELECT SSN FROM COMP_SSN WHERE COMPANY_ID IN ('"+companyId+"'))");
					metaData = query7.getMetaData();
								
					System.out.println(columnCount+" ");
					while(query7.next()) {
					    String lifeEventID = (query7.getString(4));
					    String lifeEventStatus = (query7.getString(11));
					    
				        System.out.println(lifeEventID+" "+lifeEventStatus);			 
					    }	
					
					System.out.println("Running query8...");
					
					ResultSet  query8= getResultSet(mycon, "select t.*, rowid from dependent_association t where t.ssn in (SELECT SSN FROM COMP_SSN WHERE COMPANY_ID IN ('"+companyId+"'))");
					metaData = query8.getMetaData();
								
					System.out.println(columnCount+" ");
					while(query8.next()) {
					    String lifeEventID = (query8.getString(4));
					    String lifeEventStatus = (query8.getString(11));
					    
				        System.out.println(lifeEventID+" "+lifeEventStatus);			 
					    }	
					
					System.out.println("Running query9...");
					
					ResultSet  query9= getResultSet(mycon, "select t.*, rowid from beneficiary_eff_date t where t.ssn in (SELECT SSN FROM COMP_SSN WHERE COMPANY_ID IN ('"+companyId+"'))");
					metaData = query9.getMetaData();
								
					System.out.println(columnCount+" ");
					while(query9.next()) {
					    String lifeEventID = (query9.getString(4));
					    String lifeEventStatus = (query9.getString(11));
					    
				        System.out.println(lifeEventID+" "+lifeEventStatus);			 
					    }	
					
					System.out.println("Running query10...");
					
					ResultSet  query10= getResultSet(mycon, "select t.*, rowid from beneficiary_eff_date t where t.ssn in (SELECT SSN FROM COMP_SSN WHERE COMPANY_ID IN ('"+companyId+"'))");
					metaData = query10.getMetaData();
								
					System.out.println(columnCount+" ");
					while(query10.next()) {
					    String lifeEventID = (query10.getString(4));
					    String lifeEventStatus = (query10.getString(11));
					    
				        System.out.println(lifeEventID+" "+lifeEventStatus);			 
					    }	
					
					
					System.out.println("Running query11...");
					
					ResultSet  query11= getResultSet(mycon, "select * from company_detail where company_id='"+companyId+"'");
					metaData = query11.getMetaData();
								
					System.out.println(columnCount+" ");
					while(query11.next()) {
					    String lifeEventID = (query11.getString(4));
					    String lifeEventStatus = (query11.getString(11));
					    
				        System.out.println(lifeEventID+" "+lifeEventStatus);			 
					    }	
					
					
					System.out.println("Running query12...");
					
					ResultSet  query12= getResultSet(mycon, "select * from company_payroll_config_detail where company_id='"+companyId+"'");
					metaData = query12.getMetaData();
								
					System.out.println(columnCount+" ");
					while(query12.next()) {
					    String lifeEventID = (query12.getString(4));
					    String lifeEventStatus = (query12.getString(11));
					    
				        System.out.println(lifeEventID+" "+lifeEventStatus);			 
					    }	
					
					System.out.println("Running query13...");
					
					ResultSet  query13= getResultSet(mycon, "select * from bf_election_mapping t where t.company_id ='"+companyId+"'");
					metaData = query13.getMetaData();
								
					System.out.println(columnCount+" ");
					while(query13.next()) {
					    String lifeEventID = (query13.getString(4));
					    String lifeEventStatus = (query13.getString(11));
					    
				        System.out.println(lifeEventID+" "+lifeEventStatus);			 
					    }	
					
					System.out.println("Running query14...");
					
					ResultSet  query14= getResultSet(mycon, "select * from bf_election_mapping t where t.company_id ='"+companyId+"'");
					metaData = query14.getMetaData();
								
					System.out.println(columnCount+" ");
					while(query14.next()) {
					    String lifeEventID = (query14.getString(4));
					    String lifeEventStatus = (query14.getString(11));
					    
				        System.out.println(lifeEventID+" "+lifeEventStatus);			 
					    }				
					return list;
			  }
	
}