package utilities;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFault;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.transform.stream.StreamSource;





public class SOAPUI_Request {
	/*
	 * @param soapEndpointUrl
	 * @param soapAction
	 * @param resourcePath
	 * @return responseCode
	 */

	public static String getSoapServiceResponse(String soapEndpointUrl, String soapAction, String resourcePath) throws SOAPException, IOException {
		String responseMsg = null;

		try {
			// Create SOAP Connection
			SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
			SOAPConnection soapConnection = soapConnectionFactory.createConnection();
			// Send SOAP Message to SOAP Server
			SOAPMessage soapResponse = soapConnection.call(createSOAPRequest(resourcePath, soapAction), soapEndpointUrl);
			if (!soapResponse.getSOAPBody().hasFault()) {
				System.out.println("Response SOAP Message:");
				ByteArrayOutputStream out = new ByteArrayOutputStream();
				soapResponse.writeTo(out);
				responseMsg = new String(out.toByteArray());				
				System.out.println("soap"+ soapResponse);
				System.out.println("Got Response successfully");
				soapConnection.close();
				return responseMsg;

			} else {
				SOAPFault fault = soapResponse.getSOAPBody().getFault();
				System.out.println("SOAP Fault Code :" + fault.getFaultCode());
				System.out.println("SOAP Fault String :" + fault.getFaultString());
				//throw new Exception("Something bad happened");
				return fault.getFaultCode();

			}


		} catch (Exception e) {
			System.err.println("\nError occurred while sending SOAP Request to Server!\nMake sure you have the correct endpoint URL and SOAPAction!\n");
			e.printStackTrace();
			return "";

		}

	}

	public static SOAPMessage createSOAPRequest(String resourcePath, String soapAction) throws Exception {
		MessageFactory messageFactory = MessageFactory.newInstance();
		SOAPMessage soapMessage = messageFactory.createMessage();

		SOAPPart soapPart = soapMessage.getSOAPPart();

		soapPart.setContent(new StreamSource(new FileInputStream(resourcePath)));

		MimeHeaders headers = soapMessage.getMimeHeaders();
		headers.addHeader("SOAPAction", soapAction);
		soapMessage.saveChanges();

		System.out.println("Request SOAP Message:");
		soapMessage.writeTo(System.out);
		System.out.println("\n");

		return soapMessage;
	}

}
